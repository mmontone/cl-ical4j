;;;; cl-ical4j.asd

(asdf:defsystem #:cl-ical4j
  :description "Common Lisp bindings to iCal4j Java library"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :components ((:file "package")
			   (:file "ical4j")
			   (:file "java-util")
			   (:file "java-wrappers")
			   (:file "api"))
  :depends-on (:foil))
