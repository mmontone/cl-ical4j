;;;; cl-ical4j.lisp

(in-package #:ical4j)

;;; "cl-ical4j" goes here. Hacks and glory await!

(setf foil:*fvm* (foil:make-foreign-vm))

(defparameter +jre-jar+ "/usr/lib/jvm/java-8-oracle/jre/lib/rt.jar")
(defparameter +ical4j-jar+ "/home/marian/src/lisp/jar/ical4j-2.0-beta1/ical4j-2.0-beta1.jar")

(defun generate-java-wrappers ()
  (let ((classes (remove-if (lambda (class-name)
							  (or (search "Abstract" class-name :test #'equalp)
								  (search "Factory" class-name :test #'equalp)))
							(foil:get-library-classnames +ical4j-jar+  "net/fortuna/ical4j"))))
	(foil:dump-wrapper-defs-to-file
	 (asdf:system-relative-pathname :cl-ical4j "java-wrappers.lisp")
	 classes))
  (let ((classes '("java.util.Formatter$DateTime" "java.util.SimpleTimeZone" "java.util.Timer" "java.util.TimerTask" "java.util.TimerThread"
				   "java.util.spi.TimeZoneNameProvider" "java.util.TimeZone" "java.util.Date" "java.util.Calendar$AvailableCalendarTypes"
 "java.util.Calendar$CalendarAccessControlContext"
 "java.util.JapaneseImperialCalendar" "java.util.spi.CalendarNameProvider"
 "java.util.spi.CalendarDataProvider" "java.util.GregorianCalendar"
				   "java.util.Calendar$Builder" "java.util.Calendar")))
	(foil:dump-wrapper-defs-to-file
	 (asdf:system-relative-pathname :cl-ical4j "java-util.lisp")
	 classes)))
