(in-package :ical4j)

(foil::define-java-condition illegal-argument-error (foil::java-error)
    ()
  (:java-exception "java.lang.IllegalArgumentException"))

(defstruct (recur (:print-object print-recur))
  ref)

(defstruct (datetime (:print-object print-datetime))
  ref)

(defun print-recur (recur stream)
  (format stream "<RECUR ~A>"
          (foil:to-string (recur-ref recur))))

(defun datetime-timezone (datetime)
  (foil:to-string
   (|net.fortuna.ical4j.model|:timezone.to-zone-id
                              (|net.fortuna.ical4j.model|:datetime.get-time-zone
                                                         (datetime-ref datetime)))))

(defun print-datetime (datetime stream)
  (format stream "<DATETIME ~A ~A>"
          (foil:to-string (datetime-ref datetime))
          (datetime-timezone datetime)))

(defun parse-recur (string &optional (error-p t))
  (if error-p
      (make-recur :ref
                  (|net.fortuna.ical4j.model|:recur.new string))
      (handler-case
          (make-recur :ref
                      (|net.fortuna.ical4j.model|:recur.new string))
        (illegal-argument-error ()
          (return-from parse-recur nil)))))

(defun get-timezone (string)
  (|net.fortuna.ical4j.model|:timezoneregistryimpl.get-time-zone (|net.fortuna.ical4j.model|:timezoneregistryimpl.new) string))

(defun parse-datetime (string &optional (timezone "UTC"))
  "(parse-datetime \"20151211T135948\")"
  (make-datetime :ref
                 (|net.fortuna.ical4j.model|:datetime.new
                                            string
                                            (get-timezone timezone))))

;; (parse-datetime "20151211T135948" "America/Argentina/Buenos_Aires")

(defun recur-get-dates (recur seed start end)
  (|net.fortuna.ical4j.model|:recur.get-dates
                             (recur-ref recur)
                             (datetime-ref seed)
                             (datetime-ref start)
                             (datetime-ref end)
                             (|net.fortuna.ical4j.model.parameter|:value.+date_time+)))

(defun recur-get-next-date (recur seed start)
  (let ((ref (|net.fortuna.ical4j.model|:recur.get-next-date
                                        (recur-ref recur)
                                        (datetime-ref seed)
                                        (datetime-ref start))))
    (when ref
      (make-datetime :ref ref))))

(defun datetime-before (datetime1 datetime2)
  (|net.fortuna.ical4j.model|:datetime.before
                             (datetime-ref datetime1)
                             (datetime-ref datetime2)))

(defun datetime-afeter (datetime1 datetime2)
  (|net.fortuna.ical4j.model|:datetime.after
                             (datetime-ref datetime1)
                             (datetime-ref datetime2)))

(defstruct (duration (:constructor make-duration-struct))
  ref)

(defun make-duration (start-time end-time)
  (make-duration-struct :ref
                        (|net.fortuna.ical4j.model|:dur.new
                                                   (ical4j::datetime-ref start-time)
                                                   (ical4j::datetime-ref end-time))))

(defun get-duration-time (duration start-time)
  (make-datetime :ref
                 (|net.fortuna.ical4j.model|:dur.get-time
                                            (duration-ref duration)
                                            (datetime-ref start-time))))
