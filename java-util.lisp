(eval-when (:compile-toplevel :load-toplevel)(FOIL:ENSURE-PACKAGE
                                              "java.util.spi")
(FOIL:ENSURE-PACKAGE "java.lang")
(FOIL:ENSURE-PACKAGE "java.io")
(FOIL:ENSURE-PACKAGE "java.util")
)(DEFCONSTANT |java.util|::CALENDAR. '|java.util|::|Calendar|)
(DEFCLASS |java.util|::CALENDAR.
          (|java.io|::SERIALIZABLE. |java.lang|::CLONEABLE.
           |java.lang|::COMPARABLE. |java.lang|::OBJECT.)
          NIL)
(DEFUN |java.util|::CALENDAR.SET-LENIENT (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setLenient(boolean)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "setLenient"
                          '|java.util|::CALENDAR.SET-LENIENT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.SET-TIME-IN-MILLIS (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeInMillis(long)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "setTimeInMillis"
                          '|java.util|::CALENDAR.SET-TIME-IN-MILLIS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.IS-WEEK-DATE-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isWeekDateSupported()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "isWeekDateSupported"
                          '|java.util|::CALENDAR.IS-WEEK-DATE-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-WEEK-YEAR (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getWeekYear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getWeekYear"
                          '|java.util|::CALENDAR.GET-WEEK-YEAR FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-LEAST-MAXIMUM (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int java.util.Calendar.getLeastMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getLeastMaximum"
                          '|java.util|::CALENDAR.GET-LEAST-MAXIMUM FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-DISPLAY-NAMES (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Map java.util.Calendar.getDisplayNames(int,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getDisplayNames"
                          '|java.util|::CALENDAR.GET-DISPLAY-NAMES FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.IS-LENIENT (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "isLenient"
                          '|java.util|::CALENDAR.IS-LENIENT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-FIRST-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getFirstDayOfWeek()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getFirstDayOfWeek"
                          '|java.util|::CALENDAR.GET-FIRST-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getMinimalDaysInFirstWeek()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getMinimalDaysInFirstWeek"
                          '|java.util|::CALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-AVAILABLE-CALENDAR-TYPES (&REST FOIL::ARGS)
  "public static java.util.Set java.util.Calendar.getAvailableCalendarTypes()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getAvailableCalendarTypes"
                          '|java.util|::CALENDAR.GET-AVAILABLE-CALENDAR-TYPES
                          NIL FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.ROLL (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void java.util.Calendar.roll(int,boolean)
public void java.util.Calendar.roll(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "roll"
                          '|java.util|::CALENDAR.ROLL FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.SET-FIRST-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setFirstDayOfWeek(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "setFirstDayOfWeek"
                          '|java.util|::CALENDAR.SET-FIRST-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setMinimalDaysInFirstWeek(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "setMinimalDaysInFirstWeek"
                          '|java.util|::CALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.SET-WEEK-DATE (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setWeekDate(int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "setWeekDate"
                          '|java.util|::CALENDAR.SET-WEEK-DATE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-WEEKS-IN-WEEK-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getWeeksInWeekYear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getWeeksInWeekYear"
                          '|java.util|::CALENDAR.GET-WEEKS-IN-WEEK-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-GREATEST-MINIMUM (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int java.util.Calendar.getGreatestMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getGreatestMinimum"
                          '|java.util|::CALENDAR.GET-GREATEST-MINIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-ACTUAL-MINIMUM (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getActualMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getActualMinimum"
                          '|java.util|::CALENDAR.GET-ACTUAL-MINIMUM FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-ACTUAL-MAXIMUM (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getActualMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getActualMaximum"
                          '|java.util|::CALENDAR.GET-ACTUAL-MAXIMUM FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.TO-INSTANT (FOIL::THIS &REST FOIL::ARGS)
  "public final java.time.Instant java.util.Calendar.toInstant()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "toInstant"
                          '|java.util|::CALENDAR.TO-INSTANT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-MINIMUM (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int java.util.Calendar.getMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getMinimum"
                          '|java.util|::CALENDAR.GET-MINIMUM FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-MAXIMUM (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int java.util.Calendar.getMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getMaximum"
                          '|java.util|::CALENDAR.GET-MAXIMUM FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-CALENDAR-TYPE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Calendar.getCalendarType()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getCalendarType"
                          '|java.util|::CALENDAR.GET-CALENDAR-TYPE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-TIME-IN-MILLIS (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.Calendar.getTimeInMillis()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getTimeInMillis"
                          '|java.util|::CALENDAR.GET-TIME-IN-MILLIS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-TIME-ZONE (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone java.util.Calendar.getTimeZone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getTimeZone"
                          '|java.util|::CALENDAR.GET-TIME-ZONE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.SET-TIME-ZONE (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeZone(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "setTimeZone"
                          '|java.util|::CALENDAR.SET-TIME-ZONE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.SET-TIME (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.setTime(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "setTime"
                          '|java.util|::CALENDAR.SET-TIME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-TIME (FOIL::THIS &REST FOIL::ARGS)
  "public final java.util.Date java.util.Calendar.getTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getTime"
                          '|java.util|::CALENDAR.GET-TIME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.ADD (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void java.util.Calendar.add(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "add"
                          '|java.util|::CALENDAR.ADD FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.get(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "get"
                          '|java.util|::CALENDAR.GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "equals"
                          '|java.util|::CALENDAR.EQUALS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Calendar.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "toString"
                          '|java.util|::CALENDAR.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "hashCode"
                          '|java.util|::CALENDAR.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.CLONE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Object java.util.Calendar.clone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "clone"
                          '|java.util|::CALENDAR.CLONE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.COMPARE-TO (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.compareTo(java.util.Calendar)
public int java.util.Calendar.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "compareTo"
                          '|java.util|::CALENDAR.COMPARE-TO FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.CLEAR (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.clear(int)
public final void java.util.Calendar.clear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "clear"
                          '|java.util|::CALENDAR.CLEAR FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-INSTANCE (&REST FOIL::ARGS)
  "public static java.util.Calendar java.util.Calendar.getInstance(java.util.TimeZone,java.util.Locale)
public static java.util.Calendar java.util.Calendar.getInstance()
public static java.util.Calendar java.util.Calendar.getInstance(java.util.Locale)
public static java.util.Calendar java.util.Calendar.getInstance(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getInstance"
                          '|java.util|::CALENDAR.GET-INSTANCE NIL FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.IS-SET (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean java.util.Calendar.isSet(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "isSet"
                          '|java.util|::CALENDAR.IS-SET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.SET (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.set(int,int,int,int,int,int)
public final void java.util.Calendar.set(int,int,int)
public final void java.util.Calendar.set(int,int,int,int,int)
public void java.util.Calendar.set(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "set"
                          '|java.util|::CALENDAR.SET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.BEFORE (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.before(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "before"
                          '|java.util|::CALENDAR.BEFORE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.AFTER (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.after(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "after"
                          '|java.util|::CALENDAR.AFTER FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-AVAILABLE-LOCALES (&REST FOIL::ARGS)
  "public static synchronized java.util.Locale[] java.util.Calendar.getAvailableLocales()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getAvailableLocales"
                          '|java.util|::CALENDAR.GET-AVAILABLE-LOCALES NIL
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-DISPLAY-NAME (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Calendar.getDisplayName(int,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getDisplayName"
                          '|java.util|::CALENDAR.GET-DISPLAY-NAME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "wait"
                          '|java.util|::CALENDAR.WAIT FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "getClass"
                          '|java.util|::CALENDAR.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "notify"
                          '|java.util|::CALENDAR.NOTIFY FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar| "notifyAll"
                          '|java.util|::CALENDAR.NOTIFY-ALL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.+ERA+ ()
  "public static final int java.util.Calendar.ERA"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "ERA" '|java.util|::CALENDAR.ERA*
                    NIL))
(DEFUN (SETF |java.util|::CALENDAR.+ERA+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "ERA" '|java.util|::CALENDAR.ERA*
                    NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+YEAR+ ()
  "public static final int java.util.Calendar.YEAR"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "YEAR"
                    '|java.util|::CALENDAR.YEAR* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "YEAR"
                    '|java.util|::CALENDAR.YEAR* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+MONTH+ ()
  "public static final int java.util.Calendar.MONTH"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MONTH"
                    '|java.util|::CALENDAR.MONTH* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MONTH"
                    '|java.util|::CALENDAR.MONTH* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+WEEK_OF_YEAR+ ()
  "public static final int java.util.Calendar.WEEK_OF_YEAR"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "WEEK_OF_YEAR"
                    '|java.util|::CALENDAR.WEEK_OF_YEAR* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+WEEK_OF_YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "WEEK_OF_YEAR"
                    '|java.util|::CALENDAR.WEEK_OF_YEAR* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+WEEK_OF_MONTH+ ()
  "public static final int java.util.Calendar.WEEK_OF_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "WEEK_OF_MONTH"
                    '|java.util|::CALENDAR.WEEK_OF_MONTH* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+WEEK_OF_MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "WEEK_OF_MONTH"
                    '|java.util|::CALENDAR.WEEK_OF_MONTH* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+DATE+ ()
  "public static final int java.util.Calendar.DATE"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DATE"
                    '|java.util|::CALENDAR.DATE* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+DATE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DATE"
                    '|java.util|::CALENDAR.DATE* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+DAY_OF_MONTH+ ()
  "public static final int java.util.Calendar.DAY_OF_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DAY_OF_MONTH"
                    '|java.util|::CALENDAR.DAY_OF_MONTH* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+DAY_OF_MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DAY_OF_MONTH"
                    '|java.util|::CALENDAR.DAY_OF_MONTH* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+DAY_OF_YEAR+ ()
  "public static final int java.util.Calendar.DAY_OF_YEAR"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DAY_OF_YEAR"
                    '|java.util|::CALENDAR.DAY_OF_YEAR* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+DAY_OF_YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DAY_OF_YEAR"
                    '|java.util|::CALENDAR.DAY_OF_YEAR* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+DAY_OF_WEEK+ ()
  "public static final int java.util.Calendar.DAY_OF_WEEK"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DAY_OF_WEEK"
                    '|java.util|::CALENDAR.DAY_OF_WEEK* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+DAY_OF_WEEK+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DAY_OF_WEEK"
                    '|java.util|::CALENDAR.DAY_OF_WEEK* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+DAY_OF_WEEK_IN_MONTH+ ()
  "public static final int java.util.Calendar.DAY_OF_WEEK_IN_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DAY_OF_WEEK_IN_MONTH"
                    '|java.util|::CALENDAR.DAY_OF_WEEK_IN_MONTH* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+DAY_OF_WEEK_IN_MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DAY_OF_WEEK_IN_MONTH"
                    '|java.util|::CALENDAR.DAY_OF_WEEK_IN_MONTH* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+AM_PM+ ()
  "public static final int java.util.Calendar.AM_PM"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "AM_PM"
                    '|java.util|::CALENDAR.AM_PM* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+AM_PM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "AM_PM"
                    '|java.util|::CALENDAR.AM_PM* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+HOUR+ ()
  "public static final int java.util.Calendar.HOUR"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "HOUR"
                    '|java.util|::CALENDAR.HOUR* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+HOUR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "HOUR"
                    '|java.util|::CALENDAR.HOUR* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+HOUR_OF_DAY+ ()
  "public static final int java.util.Calendar.HOUR_OF_DAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "HOUR_OF_DAY"
                    '|java.util|::CALENDAR.HOUR_OF_DAY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+HOUR_OF_DAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "HOUR_OF_DAY"
                    '|java.util|::CALENDAR.HOUR_OF_DAY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+MINUTE+ ()
  "public static final int java.util.Calendar.MINUTE"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MINUTE"
                    '|java.util|::CALENDAR.MINUTE* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+MINUTE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MINUTE"
                    '|java.util|::CALENDAR.MINUTE* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+SECOND+ ()
  "public static final int java.util.Calendar.SECOND"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SECOND"
                    '|java.util|::CALENDAR.SECOND* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+SECOND+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SECOND"
                    '|java.util|::CALENDAR.SECOND* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+MILLISECOND+ ()
  "public static final int java.util.Calendar.MILLISECOND"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MILLISECOND"
                    '|java.util|::CALENDAR.MILLISECOND* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+MILLISECOND+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MILLISECOND"
                    '|java.util|::CALENDAR.MILLISECOND* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+ZONE_OFFSET+ ()
  "public static final int java.util.Calendar.ZONE_OFFSET"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "ZONE_OFFSET"
                    '|java.util|::CALENDAR.ZONE_OFFSET* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+ZONE_OFFSET+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "ZONE_OFFSET"
                    '|java.util|::CALENDAR.ZONE_OFFSET* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+DST_OFFSET+ ()
  "public static final int java.util.Calendar.DST_OFFSET"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DST_OFFSET"
                    '|java.util|::CALENDAR.DST_OFFSET* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+DST_OFFSET+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DST_OFFSET"
                    '|java.util|::CALENDAR.DST_OFFSET* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+FIELD_COUNT+ ()
  "public static final int java.util.Calendar.FIELD_COUNT"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "FIELD_COUNT"
                    '|java.util|::CALENDAR.FIELD_COUNT* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+FIELD_COUNT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "FIELD_COUNT"
                    '|java.util|::CALENDAR.FIELD_COUNT* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+SUNDAY+ ()
  "public static final int java.util.Calendar.SUNDAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SUNDAY"
                    '|java.util|::CALENDAR.SUNDAY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+SUNDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SUNDAY"
                    '|java.util|::CALENDAR.SUNDAY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+MONDAY+ ()
  "public static final int java.util.Calendar.MONDAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MONDAY"
                    '|java.util|::CALENDAR.MONDAY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+MONDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MONDAY"
                    '|java.util|::CALENDAR.MONDAY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+TUESDAY+ ()
  "public static final int java.util.Calendar.TUESDAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "TUESDAY"
                    '|java.util|::CALENDAR.TUESDAY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+TUESDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "TUESDAY"
                    '|java.util|::CALENDAR.TUESDAY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+WEDNESDAY+ ()
  "public static final int java.util.Calendar.WEDNESDAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "WEDNESDAY"
                    '|java.util|::CALENDAR.WEDNESDAY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+WEDNESDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "WEDNESDAY"
                    '|java.util|::CALENDAR.WEDNESDAY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+THURSDAY+ ()
  "public static final int java.util.Calendar.THURSDAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "THURSDAY"
                    '|java.util|::CALENDAR.THURSDAY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+THURSDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "THURSDAY"
                    '|java.util|::CALENDAR.THURSDAY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+FRIDAY+ ()
  "public static final int java.util.Calendar.FRIDAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "FRIDAY"
                    '|java.util|::CALENDAR.FRIDAY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+FRIDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "FRIDAY"
                    '|java.util|::CALENDAR.FRIDAY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+SATURDAY+ ()
  "public static final int java.util.Calendar.SATURDAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SATURDAY"
                    '|java.util|::CALENDAR.SATURDAY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+SATURDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SATURDAY"
                    '|java.util|::CALENDAR.SATURDAY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+JANUARY+ ()
  "public static final int java.util.Calendar.JANUARY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "JANUARY"
                    '|java.util|::CALENDAR.JANUARY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+JANUARY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "JANUARY"
                    '|java.util|::CALENDAR.JANUARY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+FEBRUARY+ ()
  "public static final int java.util.Calendar.FEBRUARY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "FEBRUARY"
                    '|java.util|::CALENDAR.FEBRUARY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+FEBRUARY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "FEBRUARY"
                    '|java.util|::CALENDAR.FEBRUARY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+MARCH+ ()
  "public static final int java.util.Calendar.MARCH"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MARCH"
                    '|java.util|::CALENDAR.MARCH* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+MARCH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MARCH"
                    '|java.util|::CALENDAR.MARCH* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+APRIL+ ()
  "public static final int java.util.Calendar.APRIL"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "APRIL"
                    '|java.util|::CALENDAR.APRIL* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+APRIL+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "APRIL"
                    '|java.util|::CALENDAR.APRIL* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+MAY+ ()
  "public static final int java.util.Calendar.MAY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MAY" '|java.util|::CALENDAR.MAY*
                    NIL))
(DEFUN (SETF |java.util|::CALENDAR.+MAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "MAY" '|java.util|::CALENDAR.MAY*
                    NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+JUNE+ ()
  "public static final int java.util.Calendar.JUNE"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "JUNE"
                    '|java.util|::CALENDAR.JUNE* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+JUNE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "JUNE"
                    '|java.util|::CALENDAR.JUNE* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+JULY+ ()
  "public static final int java.util.Calendar.JULY"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "JULY"
                    '|java.util|::CALENDAR.JULY* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+JULY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "JULY"
                    '|java.util|::CALENDAR.JULY* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+AUGUST+ ()
  "public static final int java.util.Calendar.AUGUST"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "AUGUST"
                    '|java.util|::CALENDAR.AUGUST* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+AUGUST+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "AUGUST"
                    '|java.util|::CALENDAR.AUGUST* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+SEPTEMBER+ ()
  "public static final int java.util.Calendar.SEPTEMBER"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SEPTEMBER"
                    '|java.util|::CALENDAR.SEPTEMBER* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+SEPTEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SEPTEMBER"
                    '|java.util|::CALENDAR.SEPTEMBER* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+OCTOBER+ ()
  "public static final int java.util.Calendar.OCTOBER"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "OCTOBER"
                    '|java.util|::CALENDAR.OCTOBER* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+OCTOBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "OCTOBER"
                    '|java.util|::CALENDAR.OCTOBER* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+NOVEMBER+ ()
  "public static final int java.util.Calendar.NOVEMBER"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "NOVEMBER"
                    '|java.util|::CALENDAR.NOVEMBER* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+NOVEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "NOVEMBER"
                    '|java.util|::CALENDAR.NOVEMBER* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+DECEMBER+ ()
  "public static final int java.util.Calendar.DECEMBER"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DECEMBER"
                    '|java.util|::CALENDAR.DECEMBER* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+DECEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "DECEMBER"
                    '|java.util|::CALENDAR.DECEMBER* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+UNDECIMBER+ ()
  "public static final int java.util.Calendar.UNDECIMBER"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "UNDECIMBER"
                    '|java.util|::CALENDAR.UNDECIMBER* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+UNDECIMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "UNDECIMBER"
                    '|java.util|::CALENDAR.UNDECIMBER* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+AM+ ()
  "public static final int java.util.Calendar.AM"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "AM" '|java.util|::CALENDAR.AM*
                    NIL))
(DEFUN (SETF |java.util|::CALENDAR.+AM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "AM" '|java.util|::CALENDAR.AM*
                    NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+PM+ ()
  "public static final int java.util.Calendar.PM"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "PM" '|java.util|::CALENDAR.PM*
                    NIL))
(DEFUN (SETF |java.util|::CALENDAR.+PM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "PM" '|java.util|::CALENDAR.PM*
                    NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+ALL_STYLES+ ()
  "public static final int java.util.Calendar.ALL_STYLES"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "ALL_STYLES"
                    '|java.util|::CALENDAR.ALL_STYLES* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+ALL_STYLES+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "ALL_STYLES"
                    '|java.util|::CALENDAR.ALL_STYLES* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+SHORT+ ()
  "public static final int java.util.Calendar.SHORT"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SHORT"
                    '|java.util|::CALENDAR.SHORT* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+SHORT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SHORT"
                    '|java.util|::CALENDAR.SHORT* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+LONG+ ()
  "public static final int java.util.Calendar.LONG"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "LONG"
                    '|java.util|::CALENDAR.LONG* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+LONG+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "LONG"
                    '|java.util|::CALENDAR.LONG* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+NARROW_FORMAT+ ()
  "public static final int java.util.Calendar.NARROW_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "NARROW_FORMAT"
                    '|java.util|::CALENDAR.NARROW_FORMAT* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+NARROW_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "NARROW_FORMAT"
                    '|java.util|::CALENDAR.NARROW_FORMAT* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+NARROW_STANDALONE+ ()
  "public static final int java.util.Calendar.NARROW_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "NARROW_STANDALONE"
                    '|java.util|::CALENDAR.NARROW_STANDALONE* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+NARROW_STANDALONE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "NARROW_STANDALONE"
                    '|java.util|::CALENDAR.NARROW_STANDALONE* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+SHORT_FORMAT+ ()
  "public static final int java.util.Calendar.SHORT_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SHORT_FORMAT"
                    '|java.util|::CALENDAR.SHORT_FORMAT* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+SHORT_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SHORT_FORMAT"
                    '|java.util|::CALENDAR.SHORT_FORMAT* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+LONG_FORMAT+ ()
  "public static final int java.util.Calendar.LONG_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "LONG_FORMAT"
                    '|java.util|::CALENDAR.LONG_FORMAT* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+LONG_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "LONG_FORMAT"
                    '|java.util|::CALENDAR.LONG_FORMAT* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+SHORT_STANDALONE+ ()
  "public static final int java.util.Calendar.SHORT_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SHORT_STANDALONE"
                    '|java.util|::CALENDAR.SHORT_STANDALONE* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+SHORT_STANDALONE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "SHORT_STANDALONE"
                    '|java.util|::CALENDAR.SHORT_STANDALONE* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.+LONG_STANDALONE+ ()
  "public static final int java.util.Calendar.LONG_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "LONG_STANDALONE"
                    '|java.util|::CALENDAR.LONG_STANDALONE* NIL))
(DEFUN (SETF |java.util|::CALENDAR.+LONG_STANDALONE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|Calendar| "LONG_STANDALONE"
                    '|java.util|::CALENDAR.LONG_STANDALONE* NIL FOIL::VAL))
(DEFUN |java.util|::CALENDAR.CALENDAR-TYPE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Calendar.getCalendarType()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "calendarType"
                       '|java.util|::CALENDAR.CALENDARTYPE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "class"
                       '|java.util|::CALENDAR.CLASS-GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.FIRST-DAY-OF-WEEK-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getFirstDayOfWeek()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "firstDayOfWeek"
                       '|java.util|::CALENDAR.FIRSTDAYOFWEEK-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::CALENDAR.FIRST-DAY-OF-WEEK-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setFirstDayOfWeek(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Calendar| "firstDayOfWeek"
                       '|java.util|::CALENDAR.FIRSTDAYOFWEEK-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::CALENDAR.LENIENT-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isLenient()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "lenient"
                       '|java.util|::CALENDAR.LENIENT-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::CALENDAR.LENIENT-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setLenient(boolean)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Calendar| "lenient"
                       '|java.util|::CALENDAR.LENIENT-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::CALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getMinimalDaysInFirstWeek()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "minimalDaysInFirstWeek"
                       '|java.util|::CALENDAR.MINIMALDAYSINFIRSTWEEK-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::CALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setMinimalDaysInFirstWeek(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Calendar| "minimalDaysInFirstWeek"
                       '|java.util|::CALENDAR.MINIMALDAYSINFIRSTWEEK-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::CALENDAR.TIME-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.util.Date java.util.Calendar.getTime()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "time"
                       '|java.util|::CALENDAR.TIME-GET FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::CALENDAR.TIME-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.setTime(java.util.Date)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Calendar| "time"
                       '|java.util|::CALENDAR.TIME-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::CALENDAR.TIME-IN-MILLIS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.Calendar.getTimeInMillis()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "timeInMillis"
                       '|java.util|::CALENDAR.TIMEINMILLIS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::CALENDAR.TIME-IN-MILLIS-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeInMillis(long)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Calendar| "timeInMillis"
                       '|java.util|::CALENDAR.TIMEINMILLIS-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::CALENDAR.TIME-ZONE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone java.util.Calendar.getTimeZone()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "timeZone"
                       '|java.util|::CALENDAR.TIMEZONE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::CALENDAR.TIME-ZONE-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeZone(java.util.TimeZone)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Calendar| "timeZone"
                       '|java.util|::CALENDAR.TIMEZONE-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::CALENDAR.WEEK-DATE-SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isWeekDateSupported()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "weekDateSupported"
                       '|java.util|::CALENDAR.WEEKDATESUPPORTED-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.WEEK-YEAR-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getWeekYear()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "weekYear"
                       '|java.util|::CALENDAR.WEEKYEAR-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::CALENDAR.WEEKS-IN-WEEK-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getWeeksInWeekYear()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar| "weeksInWeekYear"
                       '|java.util|::CALENDAR.WEEKSINWEEKYEAR-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |java.util|::CALENDAR$AVAILABLECALENDARTYPES.
  '|java.util|::|Calendar$AvailableCalendarTypes|)
(DEFCLASS |java.util|::CALENDAR$AVAILABLECALENDARTYPES. (|java.lang|::OBJECT.)
          NIL)
(DEFUN |java.util|::CALENDAR$AVAILABLECALENDARTYPES.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$AvailableCalendarTypes|
                          "wait"
                          '|java.util|::CALENDAR$AVAILABLECALENDARTYPES.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$AVAILABLECALENDARTYPES.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$AvailableCalendarTypes|
                          "equals"
                          '|java.util|::CALENDAR$AVAILABLECALENDARTYPES.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$AVAILABLECALENDARTYPES.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$AvailableCalendarTypes|
                          "toString"
                          '|java.util|::CALENDAR$AVAILABLECALENDARTYPES.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$AVAILABLECALENDARTYPES.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$AvailableCalendarTypes|
                          "hashCode"
                          '|java.util|::CALENDAR$AVAILABLECALENDARTYPES.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$AVAILABLECALENDARTYPES.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$AvailableCalendarTypes|
                          "getClass"
                          '|java.util|::CALENDAR$AVAILABLECALENDARTYPES.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$AVAILABLECALENDARTYPES.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$AvailableCalendarTypes|
                          "notify"
                          '|java.util|::CALENDAR$AVAILABLECALENDARTYPES.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$AVAILABLECALENDARTYPES.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$AvailableCalendarTypes|
                          "notifyAll"
                          '|java.util|::CALENDAR$AVAILABLECALENDARTYPES.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$AVAILABLECALENDARTYPES.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar$AvailableCalendarTypes| "class"
                       '|java.util|::CALENDAR$AVAILABLECALENDARTYPES.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util|::CALENDAR$BUILDER. '|java.util|::|Calendar$Builder|)
(DEFCLASS |java.util|::CALENDAR$BUILDER. (|java.lang|::OBJECT.) NIL)
(DEFUN |java.util|::CALENDAR$BUILDER.NEW (&REST FOIL::ARGS)
  "public java.util.Calendar$Builder()
"
  (FOIL::CALL-CTOR '|java.util|::|Calendar$Builder| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM (EQL (QUOTE |java.util|::|Calendar$Builder|)))
            &REST FOIL::ARGS)
  (APPLY #'|java.util|::CALENDAR$BUILDER.NEW FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.BUILD (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar java.util.Calendar$Builder.build()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "build"
                          '|java.util|::CALENDAR$BUILDER.BUILD FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-LENIENT (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setLenient(boolean)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setLenient"
                          '|java.util|::CALENDAR$BUILDER.SET-LENIENT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-WEEK-DATE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setWeekDate(int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setWeekDate"
                          '|java.util|::CALENDAR$BUILDER.SET-WEEK-DATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-DATE (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setDate(int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setDate"
                          '|java.util|::CALENDAR$BUILDER.SET-DATE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-TIME-OF-DAY
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setTimeOfDay(int,int,int,int)
public java.util.Calendar$Builder java.util.Calendar$Builder.setTimeOfDay(int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setTimeOfDay"
                          '|java.util|::CALENDAR$BUILDER.SET-TIME-OF-DAY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setTimeZone(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setTimeZone"
                          '|java.util|::CALENDAR$BUILDER.SET-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-LOCALE (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setLocale(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setLocale"
                          '|java.util|::CALENDAR$BUILDER.SET-LOCALE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-INSTANT (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setInstant(java.util.Date)
public java.util.Calendar$Builder java.util.Calendar$Builder.setInstant(long)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setInstant"
                          '|java.util|::CALENDAR$BUILDER.SET-INSTANT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-FIELDS (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setFields(int[])
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setFields"
                          '|java.util|::CALENDAR$BUILDER.SET-FIELDS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-CALENDAR-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setCalendarType(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setCalendarType"
                          '|java.util|::CALENDAR$BUILDER.SET-CALENDAR-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET-WEEK-DEFINITION
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.setWeekDefinition(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "setWeekDefinition"
                          '|java.util|::CALENDAR$BUILDER.SET-WEEK-DEFINITION
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.SET (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Calendar$Builder java.util.Calendar$Builder.set(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "set"
                          '|java.util|::CALENDAR$BUILDER.SET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "wait"
                          '|java.util|::CALENDAR$BUILDER.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "equals"
                          '|java.util|::CALENDAR$BUILDER.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "toString"
                          '|java.util|::CALENDAR$BUILDER.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "hashCode"
                          '|java.util|::CALENDAR$BUILDER.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "getClass"
                          '|java.util|::CALENDAR$BUILDER.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "notify"
                          '|java.util|::CALENDAR$BUILDER.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$Builder| "notifyAll"
                          '|java.util|::CALENDAR$BUILDER.NOTIFY-ALL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$BUILDER.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar$Builder| "class"
                       '|java.util|::CALENDAR$BUILDER.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.
  '|java.util|::|Calendar$CalendarAccessControlContext|)
(DEFCLASS |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.
          (|java.lang|::OBJECT.) NIL)
(DEFUN |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.WAIT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$CalendarAccessControlContext|
                          "wait"
                          '|java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$CalendarAccessControlContext|
                          "equals"
                          '|java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$CalendarAccessControlContext|
                          "toString"
                          '|java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$CalendarAccessControlContext|
                          "hashCode"
                          '|java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$CalendarAccessControlContext|
                          "getClass"
                          '|java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$CalendarAccessControlContext|
                          "notify"
                          '|java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Calendar$CalendarAccessControlContext|
                          "notifyAll"
                          '|java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Calendar$CalendarAccessControlContext|
                       "class"
                       '|java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util|::DATE. '|java.util|::|Date|)
(DEFCLASS |java.util|::DATE.
          (|java.io|::SERIALIZABLE. |java.lang|::CLONEABLE.
           |java.lang|::COMPARABLE. |java.lang|::OBJECT.)
          NIL)
(DEFUN |java.util|::DATE.NEW (&REST FOIL::ARGS)
  "public java.util.Date()
public java.util.Date(long)
public java.util.Date(int,int,int,int,int)
public java.util.Date(int,int,int,int,int,int)
public java.util.Date(java.lang.String)
public java.util.Date(int,int,int)
"
  (FOIL::CALL-CTOR '|java.util|::|Date| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM (EQL (QUOTE |java.util|::|Date|)))
            &REST FOIL::ARGS)
  (APPLY #'|java.util|::DATE.NEW FOIL::ARGS))
(DEFUN |java.util|::DATE.UTC (&REST FOIL::ARGS)
  "public static long java.util.Date.UTC(int,int,int,int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "UTC" '|java.util|::DATE.UTC NIL
                          FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-MONTH (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getMonth()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getMonth"
                          '|java.util|::DATE.GET-MONTH FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.TO-INSTANT (FOIL::THIS &REST FOIL::ARGS)
  "public java.time.Instant java.util.Date.toInstant()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "toInstant"
                          '|java.util|::DATE.TO-INSTANT FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-SECONDS (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getSeconds()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getSeconds"
                          '|java.util|::DATE.GET-SECONDS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.SET-YEAR (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setYear(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "setYear"
                          '|java.util|::DATE.SET-YEAR FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.SET-MONTH (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setMonth(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "setMonth"
                          '|java.util|::DATE.SET-MONTH FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.SET-DATE (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setDate(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "setDate"
                          '|java.util|::DATE.SET-DATE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-DAY (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getDay()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getDay"
                          '|java.util|::DATE.GET-DAY FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-HOURS (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getHours()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getHours"
                          '|java.util|::DATE.GET-HOURS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.SET-HOURS (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setHours(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "setHours"
                          '|java.util|::DATE.SET-HOURS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-MINUTES (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getMinutes()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getMinutes"
                          '|java.util|::DATE.GET-MINUTES FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.SET-MINUTES (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setMinutes(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "setMinutes"
                          '|java.util|::DATE.SET-MINUTES FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.SET-SECONDS (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setSeconds(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "setSeconds"
                          '|java.util|::DATE.SET-SECONDS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.TO-LOCALE-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Date.toLocaleString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "toLocaleString"
                          '|java.util|::DATE.TO-LOCALE-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::DATE.TO-GMT-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Date.toGMTString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "toGMTString"
                          '|java.util|::DATE.TO-GMT-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-TIMEZONE-OFFSET (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getTimezoneOffset()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getTimezoneOffset"
                          '|java.util|::DATE.GET-TIMEZONE-OFFSET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-DATE (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getDate()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getDate"
                          '|java.util|::DATE.GET-DATE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.SET-TIME (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setTime(long)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "setTime"
                          '|java.util|::DATE.SET-TIME FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-TIME (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.Date.getTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getTime"
                          '|java.util|::DATE.GET-TIME FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.FROM (&REST FOIL::ARGS)
  "public static java.util.Date java.util.Date.from(java.time.Instant)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "from" '|java.util|::DATE.FROM
                          NIL FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-YEAR (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getYear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getYear"
                          '|java.util|::DATE.GET-YEAR FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Date.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "equals"
                          '|java.util|::DATE.EQUALS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Date.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "toString"
                          '|java.util|::DATE.TO-STRING FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "hashCode"
                          '|java.util|::DATE.HASH-CODE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.CLONE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Object java.util.Date.clone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "clone" '|java.util|::DATE.CLONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.COMPARE-TO (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.compareTo(java.lang.Object)
public int java.util.Date.compareTo(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "compareTo"
                          '|java.util|::DATE.COMPARE-TO FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.PARSE (&REST FOIL::ARGS)
  "public static long java.util.Date.parse(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "parse" '|java.util|::DATE.PARSE
                          NIL FOIL::ARGS))
(DEFUN |java.util|::DATE.BEFORE (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Date.before(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "before"
                          '|java.util|::DATE.BEFORE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.AFTER (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Date.after(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "after" '|java.util|::DATE.AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "wait" '|java.util|::DATE.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "getClass"
                          '|java.util|::DATE.GET-CLASS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "notify"
                          '|java.util|::DATE.NOTIFY FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Date| "notifyAll"
                          '|java.util|::DATE.NOTIFY-ALL FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "class"
                       '|java.util|::DATE.CLASS-GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.DATE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getDate()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "date" '|java.util|::DATE.DATE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::DATE.DATE-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setDate(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Date| "date" '|java.util|::DATE.DATE-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::DATE.DAY-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getDay()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "day" '|java.util|::DATE.DAY-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::DATE.HOURS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getHours()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "hours"
                       '|java.util|::DATE.HOURS-GET FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::DATE.HOURS-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setHours(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Date| "hours"
                       '|java.util|::DATE.HOURS-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::DATE.MINUTES-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getMinutes()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "minutes"
                       '|java.util|::DATE.MINUTES-GET FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::DATE.MINUTES-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setMinutes(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Date| "minutes"
                       '|java.util|::DATE.MINUTES-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::DATE.MONTH-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getMonth()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "month"
                       '|java.util|::DATE.MONTH-GET FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::DATE.MONTH-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setMonth(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Date| "month"
                       '|java.util|::DATE.MONTH-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::DATE.SECONDS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getSeconds()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "seconds"
                       '|java.util|::DATE.SECONDS-GET FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::DATE.SECONDS-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setSeconds(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Date| "seconds"
                       '|java.util|::DATE.SECONDS-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::DATE.TIME-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.Date.getTime()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "time" '|java.util|::DATE.TIME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::DATE.TIME-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setTime(long)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Date| "time" '|java.util|::DATE.TIME-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::DATE.TIMEZONE-OFFSET-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getTimezoneOffset()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "timezoneOffset"
                       '|java.util|::DATE.TIMEZONEOFFSET-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::DATE.YEAR-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Date.getYear()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Date| "year" '|java.util|::DATE.YEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::DATE.YEAR-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Date.setYear(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|Date| "year" '|java.util|::DATE.YEAR-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFCONSTANT |java.util|::FORMATTER$DATETIME.
  '|java.util|::|Formatter$DateTime|)
(DEFCLASS |java.util|::FORMATTER$DATETIME. (|java.lang|::OBJECT.) NIL)
(DEFUN |java.util|::FORMATTER$DATETIME.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Formatter$DateTime| "wait"
                          '|java.util|::FORMATTER$DATETIME.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::FORMATTER$DATETIME.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Formatter$DateTime| "equals"
                          '|java.util|::FORMATTER$DATETIME.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::FORMATTER$DATETIME.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Formatter$DateTime| "toString"
                          '|java.util|::FORMATTER$DATETIME.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::FORMATTER$DATETIME.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Formatter$DateTime| "hashCode"
                          '|java.util|::FORMATTER$DATETIME.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::FORMATTER$DATETIME.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Formatter$DateTime| "getClass"
                          '|java.util|::FORMATTER$DATETIME.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::FORMATTER$DATETIME.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Formatter$DateTime| "notify"
                          '|java.util|::FORMATTER$DATETIME.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::FORMATTER$DATETIME.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Formatter$DateTime| "notifyAll"
                          '|java.util|::FORMATTER$DATETIME.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::FORMATTER$DATETIME.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Formatter$DateTime| "class"
                       '|java.util|::FORMATTER$DATETIME.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFCONSTANT |java.util|::GREGORIANCALENDAR. '|java.util|::|GregorianCalendar|)
(DEFCLASS |java.util|::GREGORIANCALENDAR. (|java.util|::CALENDAR.) NIL)
(DEFUN |java.util|::GREGORIANCALENDAR.NEW (&REST FOIL::ARGS)
  "public java.util.GregorianCalendar(int,int,int)
public java.util.GregorianCalendar(int,int,int,int,int)
public java.util.GregorianCalendar(int,int,int,int,int,int)
public java.util.GregorianCalendar(java.util.TimeZone,java.util.Locale)
public java.util.GregorianCalendar(java.util.Locale)
public java.util.GregorianCalendar(java.util.TimeZone)
public java.util.GregorianCalendar()
"
  (FOIL::CALL-CTOR '|java.util|::|GregorianCalendar| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM (EQL (QUOTE |java.util|::|GregorianCalendar|)))
            &REST FOIL::ARGS)
  (APPLY #'|java.util|::GREGORIANCALENDAR.NEW FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.TO-ZONED-DATE-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.time.ZonedDateTime java.util.GregorianCalendar.toZonedDateTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "toZonedDateTime"
                          '|java.util|::GREGORIANCALENDAR.TO-ZONED-DATE-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.IS-WEEK-DATE-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean java.util.GregorianCalendar.isWeekDateSupported()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "isWeekDateSupported"
                          '|java.util|::GREGORIANCALENDAR.IS-WEEK-DATE-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-WEEK-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getWeekYear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getWeekYear"
                          '|java.util|::GREGORIANCALENDAR.GET-WEEK-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-LEAST-MAXIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getLeastMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getLeastMaximum"
                          '|java.util|::GREGORIANCALENDAR.GET-LEAST-MAXIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.ROLL (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.GregorianCalendar.roll(int,boolean)
public void java.util.GregorianCalendar.roll(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "roll"
                          '|java.util|::GREGORIANCALENDAR.ROLL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET-WEEK-DATE
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.GregorianCalendar.setWeekDate(int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "setWeekDate"
                          '|java.util|::GREGORIANCALENDAR.SET-WEEK-DATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-WEEKS-IN-WEEK-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getWeeksInWeekYear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "getWeeksInWeekYear"
                          '|java.util|::GREGORIANCALENDAR.GET-WEEKS-IN-WEEK-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-GREATEST-MINIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getGreatestMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "getGreatestMinimum"
                          '|java.util|::GREGORIANCALENDAR.GET-GREATEST-MINIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-ACTUAL-MINIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getActualMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getActualMinimum"
                          '|java.util|::GREGORIANCALENDAR.GET-ACTUAL-MINIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-ACTUAL-MAXIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getActualMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getActualMaximum"
                          '|java.util|::GREGORIANCALENDAR.GET-ACTUAL-MAXIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.IS-LEAP-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.GregorianCalendar.isLeapYear(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "isLeapYear"
                          '|java.util|::GREGORIANCALENDAR.IS-LEAP-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-MINIMUM (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getMinimum"
                          '|java.util|::GREGORIANCALENDAR.GET-MINIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-MAXIMUM (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getMaximum"
                          '|java.util|::GREGORIANCALENDAR.GET-MAXIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-CALENDAR-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.GregorianCalendar.getCalendarType()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getCalendarType"
                          '|java.util|::GREGORIANCALENDAR.GET-CALENDAR-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone java.util.GregorianCalendar.getTimeZone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getTimeZone"
                          '|java.util|::GREGORIANCALENDAR.GET-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.GregorianCalendar.setTimeZone(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "setTimeZone"
                          '|java.util|::GREGORIANCALENDAR.SET-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.FROM (&REST FOIL::ARGS)
  "public static java.util.GregorianCalendar java.util.GregorianCalendar.from(java.time.ZonedDateTime)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "from"
                          '|java.util|::GREGORIANCALENDAR.FROM NIL FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET-GREGORIAN-CHANGE
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.GregorianCalendar.setGregorianChange(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "setGregorianChange"
                          '|java.util|::GREGORIANCALENDAR.SET-GREGORIAN-CHANGE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-GREGORIAN-CHANGE
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.util.Date java.util.GregorianCalendar.getGregorianChange()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "getGregorianChange"
                          '|java.util|::GREGORIANCALENDAR.GET-GREGORIAN-CHANGE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.ADD (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.GregorianCalendar.add(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "add"
                          '|java.util|::GREGORIANCALENDAR.ADD FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.GregorianCalendar.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "equals"
                          '|java.util|::GREGORIANCALENDAR.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "hashCode"
                          '|java.util|::GREGORIANCALENDAR.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.CLONE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Object java.util.GregorianCalendar.clone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "clone"
                          '|java.util|::GREGORIANCALENDAR.CLONE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET-LENIENT (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setLenient(boolean)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "setLenient"
                          '|java.util|::GREGORIANCALENDAR.SET-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET-TIME-IN-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeInMillis(long)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "setTimeInMillis"
                          '|java.util|::GREGORIANCALENDAR.SET-TIME-IN-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-DISPLAY-NAMES
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Map java.util.Calendar.getDisplayNames(int,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getDisplayNames"
                          '|java.util|::GREGORIANCALENDAR.GET-DISPLAY-NAMES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.IS-LENIENT (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "isLenient"
                          '|java.util|::GREGORIANCALENDAR.IS-LENIENT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-FIRST-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getFirstDayOfWeek()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getFirstDayOfWeek"
                          '|java.util|::GREGORIANCALENDAR.GET-FIRST-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getMinimalDaysInFirstWeek()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "getMinimalDaysInFirstWeek"
                          '|java.util|::GREGORIANCALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-AVAILABLE-CALENDAR-TYPES
       (&REST FOIL::ARGS)
  "public static java.util.Set java.util.Calendar.getAvailableCalendarTypes()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "getAvailableCalendarTypes"
                          '|java.util|::GREGORIANCALENDAR.GET-AVAILABLE-CALENDAR-TYPES
                          NIL FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET-FIRST-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setFirstDayOfWeek(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "setFirstDayOfWeek"
                          '|java.util|::GREGORIANCALENDAR.SET-FIRST-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setMinimalDaysInFirstWeek(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "setMinimalDaysInFirstWeek"
                          '|java.util|::GREGORIANCALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.TO-INSTANT (FOIL::THIS &REST FOIL::ARGS)
  "public final java.time.Instant java.util.Calendar.toInstant()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "toInstant"
                          '|java.util|::GREGORIANCALENDAR.TO-INSTANT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-TIME-IN-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.Calendar.getTimeInMillis()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getTimeInMillis"
                          '|java.util|::GREGORIANCALENDAR.GET-TIME-IN-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET-TIME (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.setTime(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "setTime"
                          '|java.util|::GREGORIANCALENDAR.SET-TIME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-TIME (FOIL::THIS &REST FOIL::ARGS)
  "public final java.util.Date java.util.Calendar.getTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getTime"
                          '|java.util|::GREGORIANCALENDAR.GET-TIME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.get(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "get"
                          '|java.util|::GREGORIANCALENDAR.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Calendar.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "toString"
                          '|java.util|::GREGORIANCALENDAR.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.COMPARE-TO (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.compareTo(java.util.Calendar)
public int java.util.Calendar.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "compareTo"
                          '|java.util|::GREGORIANCALENDAR.COMPARE-TO FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.CLEAR (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.clear(int)
public final void java.util.Calendar.clear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "clear"
                          '|java.util|::GREGORIANCALENDAR.CLEAR FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-INSTANCE (&REST FOIL::ARGS)
  "public static java.util.Calendar java.util.Calendar.getInstance(java.util.TimeZone,java.util.Locale)
public static java.util.Calendar java.util.Calendar.getInstance()
public static java.util.Calendar java.util.Calendar.getInstance(java.util.Locale)
public static java.util.Calendar java.util.Calendar.getInstance(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getInstance"
                          '|java.util|::GREGORIANCALENDAR.GET-INSTANCE NIL
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.IS-SET (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean java.util.Calendar.isSet(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "isSet"
                          '|java.util|::GREGORIANCALENDAR.IS-SET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.SET (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.set(int,int,int,int,int,int)
public final void java.util.Calendar.set(int,int,int)
public final void java.util.Calendar.set(int,int,int,int,int)
public void java.util.Calendar.set(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "set"
                          '|java.util|::GREGORIANCALENDAR.SET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.BEFORE (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.before(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "before"
                          '|java.util|::GREGORIANCALENDAR.BEFORE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.AFTER (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.after(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "after"
                          '|java.util|::GREGORIANCALENDAR.AFTER FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-AVAILABLE-LOCALES (&REST FOIL::ARGS)
  "public static synchronized java.util.Locale[] java.util.Calendar.getAvailableLocales()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar|
                          "getAvailableLocales"
                          '|java.util|::GREGORIANCALENDAR.GET-AVAILABLE-LOCALES
                          NIL FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-DISPLAY-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Calendar.getDisplayName(int,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getDisplayName"
                          '|java.util|::GREGORIANCALENDAR.GET-DISPLAY-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "wait"
                          '|java.util|::GREGORIANCALENDAR.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "getClass"
                          '|java.util|::GREGORIANCALENDAR.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "notify"
                          '|java.util|::GREGORIANCALENDAR.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|GregorianCalendar| "notifyAll"
                          '|java.util|::GREGORIANCALENDAR.NOTIFY-ALL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.+BC+ ()
  "public static final int java.util.GregorianCalendar.BC"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "BC"
                    '|java.util|::GREGORIANCALENDAR.BC* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+BC+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "BC"
                    '|java.util|::GREGORIANCALENDAR.BC* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+AD+ ()
  "public static final int java.util.GregorianCalendar.AD"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "AD"
                    '|java.util|::GREGORIANCALENDAR.AD* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+AD+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "AD"
                    '|java.util|::GREGORIANCALENDAR.AD* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+ERA+ ()
  "public static final int java.util.Calendar.ERA"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "ERA"
                    '|java.util|::GREGORIANCALENDAR.ERA* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+ERA+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "ERA"
                    '|java.util|::GREGORIANCALENDAR.ERA* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+YEAR+ ()
  "public static final int java.util.Calendar.YEAR"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "YEAR"
                    '|java.util|::GREGORIANCALENDAR.YEAR* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "YEAR"
                    '|java.util|::GREGORIANCALENDAR.YEAR* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+MONTH+ ()
  "public static final int java.util.Calendar.MONTH"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MONTH"
                    '|java.util|::GREGORIANCALENDAR.MONTH* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MONTH"
                    '|java.util|::GREGORIANCALENDAR.MONTH* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+WEEK_OF_YEAR+ ()
  "public static final int java.util.Calendar.WEEK_OF_YEAR"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "WEEK_OF_YEAR"
                    '|java.util|::GREGORIANCALENDAR.WEEK_OF_YEAR* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+WEEK_OF_YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "WEEK_OF_YEAR"
                    '|java.util|::GREGORIANCALENDAR.WEEK_OF_YEAR* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+WEEK_OF_MONTH+ ()
  "public static final int java.util.Calendar.WEEK_OF_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "WEEK_OF_MONTH"
                    '|java.util|::GREGORIANCALENDAR.WEEK_OF_MONTH* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+WEEK_OF_MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "WEEK_OF_MONTH"
                    '|java.util|::GREGORIANCALENDAR.WEEK_OF_MONTH* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+DATE+ ()
  "public static final int java.util.Calendar.DATE"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DATE"
                    '|java.util|::GREGORIANCALENDAR.DATE* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+DATE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DATE"
                    '|java.util|::GREGORIANCALENDAR.DATE* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+DAY_OF_MONTH+ ()
  "public static final int java.util.Calendar.DAY_OF_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DAY_OF_MONTH"
                    '|java.util|::GREGORIANCALENDAR.DAY_OF_MONTH* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+DAY_OF_MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DAY_OF_MONTH"
                    '|java.util|::GREGORIANCALENDAR.DAY_OF_MONTH* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+DAY_OF_YEAR+ ()
  "public static final int java.util.Calendar.DAY_OF_YEAR"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DAY_OF_YEAR"
                    '|java.util|::GREGORIANCALENDAR.DAY_OF_YEAR* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+DAY_OF_YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DAY_OF_YEAR"
                    '|java.util|::GREGORIANCALENDAR.DAY_OF_YEAR* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+DAY_OF_WEEK+ ()
  "public static final int java.util.Calendar.DAY_OF_WEEK"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DAY_OF_WEEK"
                    '|java.util|::GREGORIANCALENDAR.DAY_OF_WEEK* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+DAY_OF_WEEK+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DAY_OF_WEEK"
                    '|java.util|::GREGORIANCALENDAR.DAY_OF_WEEK* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+DAY_OF_WEEK_IN_MONTH+ ()
  "public static final int java.util.Calendar.DAY_OF_WEEK_IN_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DAY_OF_WEEK_IN_MONTH"
                    '|java.util|::GREGORIANCALENDAR.DAY_OF_WEEK_IN_MONTH* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+DAY_OF_WEEK_IN_MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DAY_OF_WEEK_IN_MONTH"
                    '|java.util|::GREGORIANCALENDAR.DAY_OF_WEEK_IN_MONTH* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+AM_PM+ ()
  "public static final int java.util.Calendar.AM_PM"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "AM_PM"
                    '|java.util|::GREGORIANCALENDAR.AM_PM* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+AM_PM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "AM_PM"
                    '|java.util|::GREGORIANCALENDAR.AM_PM* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+HOUR+ ()
  "public static final int java.util.Calendar.HOUR"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "HOUR"
                    '|java.util|::GREGORIANCALENDAR.HOUR* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+HOUR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "HOUR"
                    '|java.util|::GREGORIANCALENDAR.HOUR* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+HOUR_OF_DAY+ ()
  "public static final int java.util.Calendar.HOUR_OF_DAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "HOUR_OF_DAY"
                    '|java.util|::GREGORIANCALENDAR.HOUR_OF_DAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+HOUR_OF_DAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "HOUR_OF_DAY"
                    '|java.util|::GREGORIANCALENDAR.HOUR_OF_DAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+MINUTE+ ()
  "public static final int java.util.Calendar.MINUTE"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MINUTE"
                    '|java.util|::GREGORIANCALENDAR.MINUTE* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+MINUTE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MINUTE"
                    '|java.util|::GREGORIANCALENDAR.MINUTE* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+SECOND+ ()
  "public static final int java.util.Calendar.SECOND"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SECOND"
                    '|java.util|::GREGORIANCALENDAR.SECOND* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+SECOND+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SECOND"
                    '|java.util|::GREGORIANCALENDAR.SECOND* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+MILLISECOND+ ()
  "public static final int java.util.Calendar.MILLISECOND"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MILLISECOND"
                    '|java.util|::GREGORIANCALENDAR.MILLISECOND* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+MILLISECOND+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MILLISECOND"
                    '|java.util|::GREGORIANCALENDAR.MILLISECOND* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+ZONE_OFFSET+ ()
  "public static final int java.util.Calendar.ZONE_OFFSET"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "ZONE_OFFSET"
                    '|java.util|::GREGORIANCALENDAR.ZONE_OFFSET* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+ZONE_OFFSET+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "ZONE_OFFSET"
                    '|java.util|::GREGORIANCALENDAR.ZONE_OFFSET* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+DST_OFFSET+ ()
  "public static final int java.util.Calendar.DST_OFFSET"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DST_OFFSET"
                    '|java.util|::GREGORIANCALENDAR.DST_OFFSET* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+DST_OFFSET+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DST_OFFSET"
                    '|java.util|::GREGORIANCALENDAR.DST_OFFSET* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+FIELD_COUNT+ ()
  "public static final int java.util.Calendar.FIELD_COUNT"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "FIELD_COUNT"
                    '|java.util|::GREGORIANCALENDAR.FIELD_COUNT* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+FIELD_COUNT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "FIELD_COUNT"
                    '|java.util|::GREGORIANCALENDAR.FIELD_COUNT* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+SUNDAY+ ()
  "public static final int java.util.Calendar.SUNDAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SUNDAY"
                    '|java.util|::GREGORIANCALENDAR.SUNDAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+SUNDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SUNDAY"
                    '|java.util|::GREGORIANCALENDAR.SUNDAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+MONDAY+ ()
  "public static final int java.util.Calendar.MONDAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MONDAY"
                    '|java.util|::GREGORIANCALENDAR.MONDAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+MONDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MONDAY"
                    '|java.util|::GREGORIANCALENDAR.MONDAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+TUESDAY+ ()
  "public static final int java.util.Calendar.TUESDAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "TUESDAY"
                    '|java.util|::GREGORIANCALENDAR.TUESDAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+TUESDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "TUESDAY"
                    '|java.util|::GREGORIANCALENDAR.TUESDAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+WEDNESDAY+ ()
  "public static final int java.util.Calendar.WEDNESDAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "WEDNESDAY"
                    '|java.util|::GREGORIANCALENDAR.WEDNESDAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+WEDNESDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "WEDNESDAY"
                    '|java.util|::GREGORIANCALENDAR.WEDNESDAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+THURSDAY+ ()
  "public static final int java.util.Calendar.THURSDAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "THURSDAY"
                    '|java.util|::GREGORIANCALENDAR.THURSDAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+THURSDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "THURSDAY"
                    '|java.util|::GREGORIANCALENDAR.THURSDAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+FRIDAY+ ()
  "public static final int java.util.Calendar.FRIDAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "FRIDAY"
                    '|java.util|::GREGORIANCALENDAR.FRIDAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+FRIDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "FRIDAY"
                    '|java.util|::GREGORIANCALENDAR.FRIDAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+SATURDAY+ ()
  "public static final int java.util.Calendar.SATURDAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SATURDAY"
                    '|java.util|::GREGORIANCALENDAR.SATURDAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+SATURDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SATURDAY"
                    '|java.util|::GREGORIANCALENDAR.SATURDAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+JANUARY+ ()
  "public static final int java.util.Calendar.JANUARY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "JANUARY"
                    '|java.util|::GREGORIANCALENDAR.JANUARY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+JANUARY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "JANUARY"
                    '|java.util|::GREGORIANCALENDAR.JANUARY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+FEBRUARY+ ()
  "public static final int java.util.Calendar.FEBRUARY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "FEBRUARY"
                    '|java.util|::GREGORIANCALENDAR.FEBRUARY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+FEBRUARY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "FEBRUARY"
                    '|java.util|::GREGORIANCALENDAR.FEBRUARY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+MARCH+ ()
  "public static final int java.util.Calendar.MARCH"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MARCH"
                    '|java.util|::GREGORIANCALENDAR.MARCH* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+MARCH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MARCH"
                    '|java.util|::GREGORIANCALENDAR.MARCH* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+APRIL+ ()
  "public static final int java.util.Calendar.APRIL"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "APRIL"
                    '|java.util|::GREGORIANCALENDAR.APRIL* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+APRIL+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "APRIL"
                    '|java.util|::GREGORIANCALENDAR.APRIL* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+MAY+ ()
  "public static final int java.util.Calendar.MAY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MAY"
                    '|java.util|::GREGORIANCALENDAR.MAY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+MAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "MAY"
                    '|java.util|::GREGORIANCALENDAR.MAY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+JUNE+ ()
  "public static final int java.util.Calendar.JUNE"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "JUNE"
                    '|java.util|::GREGORIANCALENDAR.JUNE* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+JUNE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "JUNE"
                    '|java.util|::GREGORIANCALENDAR.JUNE* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+JULY+ ()
  "public static final int java.util.Calendar.JULY"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "JULY"
                    '|java.util|::GREGORIANCALENDAR.JULY* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+JULY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "JULY"
                    '|java.util|::GREGORIANCALENDAR.JULY* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+AUGUST+ ()
  "public static final int java.util.Calendar.AUGUST"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "AUGUST"
                    '|java.util|::GREGORIANCALENDAR.AUGUST* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+AUGUST+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "AUGUST"
                    '|java.util|::GREGORIANCALENDAR.AUGUST* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+SEPTEMBER+ ()
  "public static final int java.util.Calendar.SEPTEMBER"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SEPTEMBER"
                    '|java.util|::GREGORIANCALENDAR.SEPTEMBER* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+SEPTEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SEPTEMBER"
                    '|java.util|::GREGORIANCALENDAR.SEPTEMBER* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+OCTOBER+ ()
  "public static final int java.util.Calendar.OCTOBER"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "OCTOBER"
                    '|java.util|::GREGORIANCALENDAR.OCTOBER* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+OCTOBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "OCTOBER"
                    '|java.util|::GREGORIANCALENDAR.OCTOBER* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+NOVEMBER+ ()
  "public static final int java.util.Calendar.NOVEMBER"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "NOVEMBER"
                    '|java.util|::GREGORIANCALENDAR.NOVEMBER* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+NOVEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "NOVEMBER"
                    '|java.util|::GREGORIANCALENDAR.NOVEMBER* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+DECEMBER+ ()
  "public static final int java.util.Calendar.DECEMBER"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DECEMBER"
                    '|java.util|::GREGORIANCALENDAR.DECEMBER* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+DECEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "DECEMBER"
                    '|java.util|::GREGORIANCALENDAR.DECEMBER* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+UNDECIMBER+ ()
  "public static final int java.util.Calendar.UNDECIMBER"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "UNDECIMBER"
                    '|java.util|::GREGORIANCALENDAR.UNDECIMBER* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+UNDECIMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "UNDECIMBER"
                    '|java.util|::GREGORIANCALENDAR.UNDECIMBER* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+AM+ ()
  "public static final int java.util.Calendar.AM"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "AM"
                    '|java.util|::GREGORIANCALENDAR.AM* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+AM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "AM"
                    '|java.util|::GREGORIANCALENDAR.AM* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+PM+ ()
  "public static final int java.util.Calendar.PM"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "PM"
                    '|java.util|::GREGORIANCALENDAR.PM* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+PM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "PM"
                    '|java.util|::GREGORIANCALENDAR.PM* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+ALL_STYLES+ ()
  "public static final int java.util.Calendar.ALL_STYLES"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "ALL_STYLES"
                    '|java.util|::GREGORIANCALENDAR.ALL_STYLES* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+ALL_STYLES+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "ALL_STYLES"
                    '|java.util|::GREGORIANCALENDAR.ALL_STYLES* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+SHORT+ ()
  "public static final int java.util.Calendar.SHORT"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SHORT"
                    '|java.util|::GREGORIANCALENDAR.SHORT* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+SHORT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SHORT"
                    '|java.util|::GREGORIANCALENDAR.SHORT* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+LONG+ ()
  "public static final int java.util.Calendar.LONG"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "LONG"
                    '|java.util|::GREGORIANCALENDAR.LONG* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+LONG+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "LONG"
                    '|java.util|::GREGORIANCALENDAR.LONG* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+NARROW_FORMAT+ ()
  "public static final int java.util.Calendar.NARROW_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "NARROW_FORMAT"
                    '|java.util|::GREGORIANCALENDAR.NARROW_FORMAT* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+NARROW_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "NARROW_FORMAT"
                    '|java.util|::GREGORIANCALENDAR.NARROW_FORMAT* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+NARROW_STANDALONE+ ()
  "public static final int java.util.Calendar.NARROW_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "NARROW_STANDALONE"
                    '|java.util|::GREGORIANCALENDAR.NARROW_STANDALONE* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+NARROW_STANDALONE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "NARROW_STANDALONE"
                    '|java.util|::GREGORIANCALENDAR.NARROW_STANDALONE* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+SHORT_FORMAT+ ()
  "public static final int java.util.Calendar.SHORT_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SHORT_FORMAT"
                    '|java.util|::GREGORIANCALENDAR.SHORT_FORMAT* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+SHORT_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SHORT_FORMAT"
                    '|java.util|::GREGORIANCALENDAR.SHORT_FORMAT* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+LONG_FORMAT+ ()
  "public static final int java.util.Calendar.LONG_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "LONG_FORMAT"
                    '|java.util|::GREGORIANCALENDAR.LONG_FORMAT* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+LONG_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "LONG_FORMAT"
                    '|java.util|::GREGORIANCALENDAR.LONG_FORMAT* NIL FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+SHORT_STANDALONE+ ()
  "public static final int java.util.Calendar.SHORT_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SHORT_STANDALONE"
                    '|java.util|::GREGORIANCALENDAR.SHORT_STANDALONE* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+SHORT_STANDALONE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "SHORT_STANDALONE"
                    '|java.util|::GREGORIANCALENDAR.SHORT_STANDALONE* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.+LONG_STANDALONE+ ()
  "public static final int java.util.Calendar.LONG_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "LONG_STANDALONE"
                    '|java.util|::GREGORIANCALENDAR.LONG_STANDALONE* NIL))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.+LONG_STANDALONE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|GregorianCalendar| "LONG_STANDALONE"
                    '|java.util|::GREGORIANCALENDAR.LONG_STANDALONE* NIL
                    FOIL::VAL))
(DEFUN |java.util|::GREGORIANCALENDAR.CALENDAR-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.GregorianCalendar.getCalendarType()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "calendarType"
                       '|java.util|::GREGORIANCALENDAR.CALENDARTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "class"
                       '|java.util|::GREGORIANCALENDAR.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.FIRST-DAY-OF-WEEK-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getFirstDayOfWeek()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "firstDayOfWeek"
                       '|java.util|::GREGORIANCALENDAR.FIRSTDAYOFWEEK-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.FIRST-DAY-OF-WEEK-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setFirstDayOfWeek(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|GregorianCalendar| "firstDayOfWeek"
                       '|java.util|::GREGORIANCALENDAR.FIRSTDAYOFWEEK-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::GREGORIANCALENDAR.GREGORIAN-CHANGE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.util.Date java.util.GregorianCalendar.getGregorianChange()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "gregorianChange"
                       '|java.util|::GREGORIANCALENDAR.GREGORIANCHANGE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.GREGORIAN-CHANGE-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.GregorianCalendar.setGregorianChange(java.util.Date)
"
  (FOIL::CALL-PROP-SET '|java.util|::|GregorianCalendar| "gregorianChange"
                       '|java.util|::GREGORIANCALENDAR.GREGORIANCHANGE-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::GREGORIANCALENDAR.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isLenient()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "lenient"
                       '|java.util|::GREGORIANCALENDAR.LENIENT-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.LENIENT-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setLenient(boolean)
"
  (FOIL::CALL-PROP-SET '|java.util|::|GregorianCalendar| "lenient"
                       '|java.util|::GREGORIANCALENDAR.LENIENT-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::GREGORIANCALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getMinimalDaysInFirstWeek()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar|
                       "minimalDaysInFirstWeek"
                       '|java.util|::GREGORIANCALENDAR.MINIMALDAYSINFIRSTWEEK-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setMinimalDaysInFirstWeek(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|GregorianCalendar|
                       "minimalDaysInFirstWeek"
                       '|java.util|::GREGORIANCALENDAR.MINIMALDAYSINFIRSTWEEK-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::GREGORIANCALENDAR.TIME-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.util.Date java.util.Calendar.getTime()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "time"
                       '|java.util|::GREGORIANCALENDAR.TIME-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.TIME-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.setTime(java.util.Date)
"
  (FOIL::CALL-PROP-SET '|java.util|::|GregorianCalendar| "time"
                       '|java.util|::GREGORIANCALENDAR.TIME-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::GREGORIANCALENDAR.TIME-IN-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.Calendar.getTimeInMillis()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "timeInMillis"
                       '|java.util|::GREGORIANCALENDAR.TIMEINMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.TIME-IN-MILLIS-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeInMillis(long)
"
  (FOIL::CALL-PROP-SET '|java.util|::|GregorianCalendar| "timeInMillis"
                       '|java.util|::GREGORIANCALENDAR.TIMEINMILLIS-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::GREGORIANCALENDAR.TIME-ZONE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone java.util.GregorianCalendar.getTimeZone()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "timeZone"
                       '|java.util|::GREGORIANCALENDAR.TIMEZONE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::GREGORIANCALENDAR.TIME-ZONE-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.GregorianCalendar.setTimeZone(java.util.TimeZone)
"
  (FOIL::CALL-PROP-SET '|java.util|::|GregorianCalendar| "timeZone"
                       '|java.util|::GREGORIANCALENDAR.TIMEZONE-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::GREGORIANCALENDAR.WEEK-DATE-SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean java.util.GregorianCalendar.isWeekDateSupported()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "weekDateSupported"
                       '|java.util|::GREGORIANCALENDAR.WEEKDATESUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.WEEK-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getWeekYear()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "weekYear"
                       '|java.util|::GREGORIANCALENDAR.WEEKYEAR-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::GREGORIANCALENDAR.WEEKS-IN-WEEK-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.GregorianCalendar.getWeeksInWeekYear()
"
  (FOIL::CALL-PROP-GET '|java.util|::|GregorianCalendar| "weeksInWeekYear"
                       '|java.util|::GREGORIANCALENDAR.WEEKSINWEEKYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util|::JAPANESEIMPERIALCALENDAR.
  '|java.util|::|JapaneseImperialCalendar|)
(DEFCLASS |java.util|::JAPANESEIMPERIALCALENDAR. (|java.util|::CALENDAR.) NIL)
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-LEAST-MAXIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.JapaneseImperialCalendar.getLeastMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getLeastMaximum"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-LEAST-MAXIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-DISPLAY-NAMES
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.Map java.util.JapaneseImperialCalendar.getDisplayNames(int,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getDisplayNames"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-DISPLAY-NAMES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.ROLL (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.JapaneseImperialCalendar.roll(int,int)
public void java.util.JapaneseImperialCalendar.roll(int,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "roll"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.ROLL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-GREATEST-MINIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.JapaneseImperialCalendar.getGreatestMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getGreatestMinimum"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-GREATEST-MINIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-ACTUAL-MINIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.JapaneseImperialCalendar.getActualMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getActualMinimum"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-ACTUAL-MINIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-ACTUAL-MAXIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.JapaneseImperialCalendar.getActualMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getActualMaximum"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-ACTUAL-MAXIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-MINIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.JapaneseImperialCalendar.getMinimum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "getMinimum"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-MINIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-MAXIMUM
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.JapaneseImperialCalendar.getMaximum(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "getMaximum"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-MAXIMUM
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-CALENDAR-TYPE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.JapaneseImperialCalendar.getCalendarType()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getCalendarType"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-CALENDAR-TYPE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone java.util.JapaneseImperialCalendar.getTimeZone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getTimeZone"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME-ZONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.JapaneseImperialCalendar.setTimeZone(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "setTimeZone"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME-ZONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.ADD (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.JapaneseImperialCalendar.add(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "add"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.ADD FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.JapaneseImperialCalendar.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "equals"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.JapaneseImperialCalendar.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "hashCode"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.CLONE
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Object java.util.JapaneseImperialCalendar.clone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "clone"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.CLONE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-DISPLAY-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.JapaneseImperialCalendar.getDisplayName(int,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getDisplayName"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-DISPLAY-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.SET-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setLenient(boolean)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "setLenient"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.SET-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME-IN-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeInMillis(long)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "setTimeInMillis"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME-IN-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.IS-WEEK-DATE-SUPPORTED
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isWeekDateSupported()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "isWeekDateSupported"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.IS-WEEK-DATE-SUPPORTED
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-WEEK-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getWeekYear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getWeekYear"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-WEEK-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.IS-LENIENT
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isLenient()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "isLenient"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.IS-LENIENT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-FIRST-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getFirstDayOfWeek()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getFirstDayOfWeek"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-FIRST-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getMinimalDaysInFirstWeek()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getMinimalDaysInFirstWeek"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-AVAILABLE-CALENDAR-TYPES
       (&REST FOIL::ARGS)
  "public static java.util.Set java.util.Calendar.getAvailableCalendarTypes()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getAvailableCalendarTypes"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-AVAILABLE-CALENDAR-TYPES
                          NIL FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.SET-FIRST-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setFirstDayOfWeek(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "setFirstDayOfWeek"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.SET-FIRST-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setMinimalDaysInFirstWeek(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "setMinimalDaysInFirstWeek"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.SET-WEEK-DATE
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setWeekDate(int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "setWeekDate"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.SET-WEEK-DATE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-WEEKS-IN-WEEK-YEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getWeeksInWeekYear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getWeeksInWeekYear"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-WEEKS-IN-WEEK-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.TO-INSTANT
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.time.Instant java.util.Calendar.toInstant()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "toInstant"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.TO-INSTANT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME-IN-MILLIS
       (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.Calendar.getTimeInMillis()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getTimeInMillis"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME-IN-MILLIS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.setTime(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "setTime"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.util.Date java.util.Calendar.getTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "getTime"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.get(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "get"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Calendar.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "toString"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.COMPARE-TO
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.compareTo(java.util.Calendar)
public int java.util.Calendar.compareTo(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "compareTo"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.COMPARE-TO
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.CLEAR
       (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.clear(int)
public final void java.util.Calendar.clear()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "clear"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.CLEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-INSTANCE (&REST FOIL::ARGS)
  "public static java.util.Calendar java.util.Calendar.getInstance(java.util.TimeZone,java.util.Locale)
public static java.util.Calendar java.util.Calendar.getInstance()
public static java.util.Calendar java.util.Calendar.getInstance(java.util.Locale)
public static java.util.Calendar java.util.Calendar.getInstance(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getInstance"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-INSTANCE
                          NIL FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.IS-SET
       (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean java.util.Calendar.isSet(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "isSet"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.IS-SET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.SET (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.set(int,int,int,int,int,int)
public final void java.util.Calendar.set(int,int,int)
public final void java.util.Calendar.set(int,int,int,int,int)
public void java.util.Calendar.set(int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "set"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.SET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.BEFORE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.before(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "before"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.BEFORE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.AFTER
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.after(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "after"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.AFTER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-AVAILABLE-LOCALES
       (&REST FOIL::ARGS)
  "public static synchronized java.util.Locale[] java.util.Calendar.getAvailableLocales()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar|
                          "getAvailableLocales"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-AVAILABLE-LOCALES
                          NIL FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "wait"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "getClass"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "notify"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|JapaneseImperialCalendar| "notifyAll"
                          '|java.util|::JAPANESEIMPERIALCALENDAR.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+BEFORE_MEIJI+ ()
  "public static final int java.util.JapaneseImperialCalendar.BEFORE_MEIJI"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "BEFORE_MEIJI"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.BEFORE_MEIJI* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+BEFORE_MEIJI+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "BEFORE_MEIJI"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.BEFORE_MEIJI* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+MEIJI+ ()
  "public static final int java.util.JapaneseImperialCalendar.MEIJI"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MEIJI"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MEIJI* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+MEIJI+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MEIJI"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MEIJI* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+TAISHO+ ()
  "public static final int java.util.JapaneseImperialCalendar.TAISHO"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "TAISHO"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.TAISHO* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+TAISHO+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "TAISHO"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.TAISHO* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+SHOWA+ ()
  "public static final int java.util.JapaneseImperialCalendar.SHOWA"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SHOWA"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SHOWA* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+SHOWA+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SHOWA"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SHOWA* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+HEISEI+ ()
  "public static final int java.util.JapaneseImperialCalendar.HEISEI"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "HEISEI"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.HEISEI* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+HEISEI+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "HEISEI"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.HEISEI* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+ERA+ ()
  "public static final int java.util.Calendar.ERA"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "ERA"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.ERA* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+ERA+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "ERA"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.ERA* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+YEAR+ ()
  "public static final int java.util.Calendar.YEAR"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "YEAR"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.YEAR* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "YEAR"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.YEAR* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+MONTH+ ()
  "public static final int java.util.Calendar.MONTH"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MONTH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MONTH* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MONTH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MONTH* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+WEEK_OF_YEAR+ ()
  "public static final int java.util.Calendar.WEEK_OF_YEAR"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "WEEK_OF_YEAR"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.WEEK_OF_YEAR* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+WEEK_OF_YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "WEEK_OF_YEAR"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.WEEK_OF_YEAR* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+WEEK_OF_MONTH+ ()
  "public static final int java.util.Calendar.WEEK_OF_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "WEEK_OF_MONTH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.WEEK_OF_MONTH* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+WEEK_OF_MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "WEEK_OF_MONTH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.WEEK_OF_MONTH* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+DATE+ ()
  "public static final int java.util.Calendar.DATE"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DATE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DATE* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+DATE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DATE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DATE* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_MONTH+ ()
  "public static final int java.util.Calendar.DAY_OF_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DAY_OF_MONTH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DAY_OF_MONTH* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_MONTH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DAY_OF_MONTH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DAY_OF_MONTH* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_YEAR+ ()
  "public static final int java.util.Calendar.DAY_OF_YEAR"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DAY_OF_YEAR"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DAY_OF_YEAR* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_YEAR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DAY_OF_YEAR"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DAY_OF_YEAR* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_WEEK+ ()
  "public static final int java.util.Calendar.DAY_OF_WEEK"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DAY_OF_WEEK"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DAY_OF_WEEK* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_WEEK+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DAY_OF_WEEK"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DAY_OF_WEEK* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_WEEK_IN_MONTH+ ()
  "public static final int java.util.Calendar.DAY_OF_WEEK_IN_MONTH"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar|
                    "DAY_OF_WEEK_IN_MONTH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DAY_OF_WEEK_IN_MONTH*
                    NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_WEEK_IN_MONTH+)
       (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar|
                    "DAY_OF_WEEK_IN_MONTH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DAY_OF_WEEK_IN_MONTH*
                    NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+AM_PM+ ()
  "public static final int java.util.Calendar.AM_PM"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "AM_PM"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.AM_PM* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+AM_PM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "AM_PM"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.AM_PM* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+HOUR+ ()
  "public static final int java.util.Calendar.HOUR"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "HOUR"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.HOUR* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+HOUR+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "HOUR"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.HOUR* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+HOUR_OF_DAY+ ()
  "public static final int java.util.Calendar.HOUR_OF_DAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "HOUR_OF_DAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.HOUR_OF_DAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+HOUR_OF_DAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "HOUR_OF_DAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.HOUR_OF_DAY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+MINUTE+ ()
  "public static final int java.util.Calendar.MINUTE"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MINUTE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MINUTE* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+MINUTE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MINUTE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MINUTE* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+SECOND+ ()
  "public static final int java.util.Calendar.SECOND"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SECOND"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SECOND* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+SECOND+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SECOND"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SECOND* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+MILLISECOND+ ()
  "public static final int java.util.Calendar.MILLISECOND"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MILLISECOND"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MILLISECOND* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+MILLISECOND+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MILLISECOND"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MILLISECOND* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+ZONE_OFFSET+ ()
  "public static final int java.util.Calendar.ZONE_OFFSET"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "ZONE_OFFSET"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.ZONE_OFFSET* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+ZONE_OFFSET+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "ZONE_OFFSET"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.ZONE_OFFSET* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+DST_OFFSET+ ()
  "public static final int java.util.Calendar.DST_OFFSET"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DST_OFFSET"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DST_OFFSET* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+DST_OFFSET+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DST_OFFSET"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DST_OFFSET* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+FIELD_COUNT+ ()
  "public static final int java.util.Calendar.FIELD_COUNT"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "FIELD_COUNT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.FIELD_COUNT* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+FIELD_COUNT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "FIELD_COUNT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.FIELD_COUNT* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+SUNDAY+ ()
  "public static final int java.util.Calendar.SUNDAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SUNDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SUNDAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+SUNDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SUNDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SUNDAY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+MONDAY+ ()
  "public static final int java.util.Calendar.MONDAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MONDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MONDAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+MONDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MONDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MONDAY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+TUESDAY+ ()
  "public static final int java.util.Calendar.TUESDAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "TUESDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.TUESDAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+TUESDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "TUESDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.TUESDAY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+WEDNESDAY+ ()
  "public static final int java.util.Calendar.WEDNESDAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "WEDNESDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.WEDNESDAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+WEDNESDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "WEDNESDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.WEDNESDAY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+THURSDAY+ ()
  "public static final int java.util.Calendar.THURSDAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "THURSDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.THURSDAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+THURSDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "THURSDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.THURSDAY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+FRIDAY+ ()
  "public static final int java.util.Calendar.FRIDAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "FRIDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.FRIDAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+FRIDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "FRIDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.FRIDAY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+SATURDAY+ ()
  "public static final int java.util.Calendar.SATURDAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SATURDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SATURDAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+SATURDAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SATURDAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SATURDAY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+JANUARY+ ()
  "public static final int java.util.Calendar.JANUARY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "JANUARY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.JANUARY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+JANUARY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "JANUARY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.JANUARY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+FEBRUARY+ ()
  "public static final int java.util.Calendar.FEBRUARY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "FEBRUARY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.FEBRUARY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+FEBRUARY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "FEBRUARY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.FEBRUARY* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+MARCH+ ()
  "public static final int java.util.Calendar.MARCH"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MARCH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MARCH* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+MARCH+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MARCH"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MARCH* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+APRIL+ ()
  "public static final int java.util.Calendar.APRIL"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "APRIL"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.APRIL* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+APRIL+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "APRIL"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.APRIL* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+MAY+ ()
  "public static final int java.util.Calendar.MAY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MAY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+MAY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "MAY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.MAY* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+JUNE+ ()
  "public static final int java.util.Calendar.JUNE"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "JUNE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.JUNE* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+JUNE+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "JUNE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.JUNE* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+JULY+ ()
  "public static final int java.util.Calendar.JULY"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "JULY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.JULY* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+JULY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "JULY"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.JULY* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+AUGUST+ ()
  "public static final int java.util.Calendar.AUGUST"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "AUGUST"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.AUGUST* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+AUGUST+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "AUGUST"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.AUGUST* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+SEPTEMBER+ ()
  "public static final int java.util.Calendar.SEPTEMBER"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SEPTEMBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SEPTEMBER* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+SEPTEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SEPTEMBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SEPTEMBER* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+OCTOBER+ ()
  "public static final int java.util.Calendar.OCTOBER"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "OCTOBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.OCTOBER* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+OCTOBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "OCTOBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.OCTOBER* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+NOVEMBER+ ()
  "public static final int java.util.Calendar.NOVEMBER"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "NOVEMBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.NOVEMBER* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+NOVEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "NOVEMBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.NOVEMBER* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+DECEMBER+ ()
  "public static final int java.util.Calendar.DECEMBER"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DECEMBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DECEMBER* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+DECEMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "DECEMBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.DECEMBER* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+UNDECIMBER+ ()
  "public static final int java.util.Calendar.UNDECIMBER"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "UNDECIMBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.UNDECIMBER* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+UNDECIMBER+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "UNDECIMBER"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.UNDECIMBER* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+AM+ ()
  "public static final int java.util.Calendar.AM"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "AM"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.AM* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+AM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "AM"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.AM* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+PM+ ()
  "public static final int java.util.Calendar.PM"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "PM"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.PM* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+PM+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "PM"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.PM* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+ALL_STYLES+ ()
  "public static final int java.util.Calendar.ALL_STYLES"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "ALL_STYLES"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.ALL_STYLES* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+ALL_STYLES+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "ALL_STYLES"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.ALL_STYLES* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT+ ()
  "public static final int java.util.Calendar.SHORT"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SHORT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SHORT* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SHORT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SHORT* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+LONG+ ()
  "public static final int java.util.Calendar.LONG"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "LONG"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.LONG* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+LONG+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "LONG"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.LONG* NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+NARROW_FORMAT+ ()
  "public static final int java.util.Calendar.NARROW_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "NARROW_FORMAT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.NARROW_FORMAT* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+NARROW_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "NARROW_FORMAT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.NARROW_FORMAT* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+NARROW_STANDALONE+ ()
  "public static final int java.util.Calendar.NARROW_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar|
                    "NARROW_STANDALONE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.NARROW_STANDALONE*
                    NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+NARROW_STANDALONE+)
       (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar|
                    "NARROW_STANDALONE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.NARROW_STANDALONE*
                    NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT_FORMAT+ ()
  "public static final int java.util.Calendar.SHORT_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SHORT_FORMAT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SHORT_FORMAT* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SHORT_FORMAT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SHORT_FORMAT* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+LONG_FORMAT+ ()
  "public static final int java.util.Calendar.LONG_FORMAT"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "LONG_FORMAT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.LONG_FORMAT* NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+LONG_FORMAT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "LONG_FORMAT"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.LONG_FORMAT* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT_STANDALONE+ ()
  "public static final int java.util.Calendar.SHORT_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SHORT_STANDALONE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SHORT_STANDALONE*
                    NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT_STANDALONE+)
       (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "SHORT_STANDALONE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.SHORT_STANDALONE*
                    NIL FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.+LONG_STANDALONE+ ()
  "public static final int java.util.Calendar.LONG_STANDALONE"
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "LONG_STANDALONE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.LONG_STANDALONE*
                    NIL))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.+LONG_STANDALONE+)
       (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|JapaneseImperialCalendar| "LONG_STANDALONE"
                    '|java.util|::JAPANESEIMPERIALCALENDAR.LONG_STANDALONE* NIL
                    FOIL::VAL))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.CALENDAR-TYPE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.Calendar.getCalendarType()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar| "calendarType"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.CALENDARTYPE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar| "class"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.FIRST-DAY-OF-WEEK-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getFirstDayOfWeek()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar|
                       "firstDayOfWeek"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.FIRSTDAYOFWEEK-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.FIRST-DAY-OF-WEEK-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setFirstDayOfWeek(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|JapaneseImperialCalendar|
                       "firstDayOfWeek"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.FIRSTDAYOFWEEK-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.LENIENT-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isLenient()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar| "lenient"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.LENIENT-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.LENIENT-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setLenient(boolean)
"
  (FOIL::CALL-PROP-SET '|java.util|::|JapaneseImperialCalendar| "lenient"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.LENIENT-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getMinimalDaysInFirstWeek()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar|
                       "minimalDaysInFirstWeek"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.MINIMALDAYSINFIRSTWEEK-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setMinimalDaysInFirstWeek(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|JapaneseImperialCalendar|
                       "minimalDaysInFirstWeek"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.MINIMALDAYSINFIRSTWEEK-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.TIME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.util.Date java.util.Calendar.getTime()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar| "time"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.TIME-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.TIME-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public final void java.util.Calendar.setTime(java.util.Date)
"
  (FOIL::CALL-PROP-SET '|java.util|::|JapaneseImperialCalendar| "time"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.TIME-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.TIME-IN-MILLIS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.Calendar.getTimeInMillis()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar| "timeInMillis"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.TIMEINMILLIS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.TIME-IN-MILLIS-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeInMillis(long)
"
  (FOIL::CALL-PROP-SET '|java.util|::|JapaneseImperialCalendar| "timeInMillis"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.TIMEINMILLIS-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.TIME-ZONE-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.util.TimeZone java.util.Calendar.getTimeZone()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar| "timeZone"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.TIMEZONE-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::JAPANESEIMPERIALCALENDAR.TIME-ZONE-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Calendar.setTimeZone(java.util.TimeZone)
"
  (FOIL::CALL-PROP-SET '|java.util|::|JapaneseImperialCalendar| "timeZone"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.TIMEZONE-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.WEEK-DATE-SUPPORTED-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.Calendar.isWeekDateSupported()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar|
                       "weekDateSupported"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.WEEKDATESUPPORTED-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.WEEK-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getWeekYear()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar| "weekYear"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.WEEKYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::JAPANESEIMPERIALCALENDAR.WEEKS-IN-WEEK-YEAR-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Calendar.getWeeksInWeekYear()
"
  (FOIL::CALL-PROP-GET '|java.util|::|JapaneseImperialCalendar|
                       "weeksInWeekYear"
                       '|java.util|::JAPANESEIMPERIALCALENDAR.WEEKSINWEEKYEAR-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util|::SIMPLETIMEZONE. '|java.util|::|SimpleTimeZone|)
(DEFCLASS |java.util|::SIMPLETIMEZONE. (|java.util|::TIMEZONE.) NIL)
(DEFUN |java.util|::SIMPLETIMEZONE.NEW (&REST FOIL::ARGS)
  "public java.util.SimpleTimeZone(int,java.lang.String)
public java.util.SimpleTimeZone(int,java.lang.String,int,int,int,int,int,int,int,int,int)
public java.util.SimpleTimeZone(int,java.lang.String,int,int,int,int,int,int,int,int)
public java.util.SimpleTimeZone(int,java.lang.String,int,int,int,int,int,int,int,int,int,int,int)
"
  (FOIL::CALL-CTOR '|java.util|::|SimpleTimeZone| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM (EQL (QUOTE |java.util|::|SimpleTimeZone|)))
            &REST FOIL::ARGS)
  (APPLY #'|java.util|::SIMPLETIMEZONE.NEW FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.IN-DAYLIGHT-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.SimpleTimeZone.inDaylightTime(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "inDaylightTime"
                          '|java.util|::SIMPLETIMEZONE.IN-DAYLIGHT-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-RAW-OFFSET (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.SimpleTimeZone.getRawOffset()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getRawOffset"
                          '|java.util|::SIMPLETIMEZONE.GET-RAW-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-DST-SAVINGS
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.SimpleTimeZone.getDSTSavings()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getDSTSavings"
                          '|java.util|::SIMPLETIMEZONE.GET-DST-SAVINGS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.HAS-SAME-RULES (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.SimpleTimeZone.hasSameRules(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "hasSameRules"
                          '|java.util|::SIMPLETIMEZONE.HAS-SAME-RULES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.SET-RAW-OFFSET (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.SimpleTimeZone.setRawOffset(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "setRawOffset"
                          '|java.util|::SIMPLETIMEZONE.SET-RAW-OFFSET
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.USE-DAYLIGHT-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.SimpleTimeZone.useDaylightTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "useDaylightTime"
                          '|java.util|::SIMPLETIMEZONE.USE-DAYLIGHT-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.OBSERVES-DAYLIGHT-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.SimpleTimeZone.observesDaylightTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "observesDaylightTime"
                          '|java.util|::SIMPLETIMEZONE.OBSERVES-DAYLIGHT-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.SET-START-YEAR (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.SimpleTimeZone.setStartYear(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "setStartYear"
                          '|java.util|::SIMPLETIMEZONE.SET-START-YEAR
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.SET-START-RULE (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.SimpleTimeZone.setStartRule(int,int,int,int,boolean)
public void java.util.SimpleTimeZone.setStartRule(int,int,int)
public void java.util.SimpleTimeZone.setStartRule(int,int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "setStartRule"
                          '|java.util|::SIMPLETIMEZONE.SET-START-RULE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.SET-END-RULE (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.SimpleTimeZone.setEndRule(int,int,int)
public void java.util.SimpleTimeZone.setEndRule(int,int,int,int)
public void java.util.SimpleTimeZone.setEndRule(int,int,int,int,boolean)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "setEndRule"
                          '|java.util|::SIMPLETIMEZONE.SET-END-RULE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.SET-DST-SAVINGS
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.SimpleTimeZone.setDSTSavings(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "setDSTSavings"
                          '|java.util|::SIMPLETIMEZONE.SET-DST-SAVINGS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.SimpleTimeZone.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "equals"
                          '|java.util|::SIMPLETIMEZONE.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.SimpleTimeZone.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "toString"
                          '|java.util|::SIMPLETIMEZONE.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public synchronized int java.util.SimpleTimeZone.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "hashCode"
                          '|java.util|::SIMPLETIMEZONE.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.CLONE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Object java.util.SimpleTimeZone.clone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "clone"
                          '|java.util|::SIMPLETIMEZONE.CLONE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-OFFSET (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.SimpleTimeZone.getOffset(int,int,int,int,int,int)
public int java.util.SimpleTimeZone.getOffset(long)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getOffset"
                          '|java.util|::SIMPLETIMEZONE.GET-OFFSET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.SET-ID (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.TimeZone.setID(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "setID"
                          '|java.util|::SIMPLETIMEZONE.SET-ID FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-AVAILABLE-I-DS (&REST FOIL::ARGS)
  "public static synchronized java.lang.String[] java.util.TimeZone.getAvailableIDs()
public static synchronized java.lang.String[] java.util.TimeZone.getAvailableIDs(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getAvailableIDs"
                          '|java.util|::SIMPLETIMEZONE.GET-AVAILABLE-I-DS NIL
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.TO-ZONE-ID (FOIL::THIS &REST FOIL::ARGS)
  "public java.time.ZoneId java.util.TimeZone.toZoneId()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "toZoneId"
                          '|java.util|::SIMPLETIMEZONE.TO-ZONE-ID FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-TIME-ZONE (&REST FOIL::ARGS)
  "public static synchronized java.util.TimeZone java.util.TimeZone.getTimeZone(java.lang.String)
public static java.util.TimeZone java.util.TimeZone.getTimeZone(java.time.ZoneId)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getTimeZone"
                          '|java.util|::SIMPLETIMEZONE.GET-TIME-ZONE NIL
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-DEFAULT (&REST FOIL::ARGS)
  "public static java.util.TimeZone java.util.TimeZone.getDefault()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getDefault"
                          '|java.util|::SIMPLETIMEZONE.GET-DEFAULT NIL
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.SET-DEFAULT (&REST FOIL::ARGS)
  "public static void java.util.TimeZone.setDefault(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "setDefault"
                          '|java.util|::SIMPLETIMEZONE.SET-DEFAULT NIL
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-DISPLAY-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.TimeZone.getDisplayName(boolean,int,java.util.Locale)
public final java.lang.String java.util.TimeZone.getDisplayName()
public final java.lang.String java.util.TimeZone.getDisplayName(java.util.Locale)
public final java.lang.String java.util.TimeZone.getDisplayName(boolean,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getDisplayName"
                          '|java.util|::SIMPLETIMEZONE.GET-DISPLAY-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-ID (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.TimeZone.getID()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getID"
                          '|java.util|::SIMPLETIMEZONE.GET-ID FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "wait"
                          '|java.util|::SIMPLETIMEZONE.WAIT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "getClass"
                          '|java.util|::SIMPLETIMEZONE.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "notify"
                          '|java.util|::SIMPLETIMEZONE.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|SimpleTimeZone| "notifyAll"
                          '|java.util|::SIMPLETIMEZONE.NOTIFY-ALL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.+WALL_TIME+ ()
  "public static final int java.util.SimpleTimeZone.WALL_TIME"
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "WALL_TIME"
                    '|java.util|::SIMPLETIMEZONE.WALL_TIME* NIL))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.+WALL_TIME+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "WALL_TIME"
                    '|java.util|::SIMPLETIMEZONE.WALL_TIME* NIL FOIL::VAL))
(DEFUN |java.util|::SIMPLETIMEZONE.+STANDARD_TIME+ ()
  "public static final int java.util.SimpleTimeZone.STANDARD_TIME"
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "STANDARD_TIME"
                    '|java.util|::SIMPLETIMEZONE.STANDARD_TIME* NIL))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.+STANDARD_TIME+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "STANDARD_TIME"
                    '|java.util|::SIMPLETIMEZONE.STANDARD_TIME* NIL FOIL::VAL))
(DEFUN |java.util|::SIMPLETIMEZONE.+UTC_TIME+ ()
  "public static final int java.util.SimpleTimeZone.UTC_TIME"
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "UTC_TIME"
                    '|java.util|::SIMPLETIMEZONE.UTC_TIME* NIL))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.+UTC_TIME+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "UTC_TIME"
                    '|java.util|::SIMPLETIMEZONE.UTC_TIME* NIL FOIL::VAL))
(DEFUN |java.util|::SIMPLETIMEZONE.+SHORT+ ()
  "public static final int java.util.TimeZone.SHORT"
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "SHORT"
                    '|java.util|::SIMPLETIMEZONE.SHORT* NIL))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.+SHORT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "SHORT"
                    '|java.util|::SIMPLETIMEZONE.SHORT* NIL FOIL::VAL))
(DEFUN |java.util|::SIMPLETIMEZONE.+LONG+ ()
  "public static final int java.util.TimeZone.LONG"
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "LONG"
                    '|java.util|::SIMPLETIMEZONE.LONG* NIL))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.+LONG+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|SimpleTimeZone| "LONG"
                    '|java.util|::SIMPLETIMEZONE.LONG* NIL FOIL::VAL))
(DEFUN |java.util|::SIMPLETIMEZONE.DST-SAVINGS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.SimpleTimeZone.getDSTSavings()
"
  (FOIL::CALL-PROP-GET '|java.util|::|SimpleTimeZone| "DSTSavings"
                       '|java.util|::SIMPLETIMEZONE.DSTSAVINGS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.DST-SAVINGS-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.SimpleTimeZone.setDSTSavings(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|SimpleTimeZone| "DSTSavings"
                       '|java.util|::SIMPLETIMEZONE.DSTSAVINGS-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::SIMPLETIMEZONE.ID-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.TimeZone.getID()
"
  (FOIL::CALL-PROP-GET '|java.util|::|SimpleTimeZone| "ID"
                       '|java.util|::SIMPLETIMEZONE.ID-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.ID-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.TimeZone.setID(java.lang.String)
"
  (FOIL::CALL-PROP-SET '|java.util|::|SimpleTimeZone| "ID"
                       '|java.util|::SIMPLETIMEZONE.ID-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::SIMPLETIMEZONE.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|SimpleTimeZone| "class"
                       '|java.util|::SIMPLETIMEZONE.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.DISPLAY-NAME-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.TimeZone.getDisplayName()
"
  (FOIL::CALL-PROP-GET '|java.util|::|SimpleTimeZone| "displayName"
                       '|java.util|::SIMPLETIMEZONE.DISPLAYNAME-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::SIMPLETIMEZONE.RAW-OFFSET-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.SimpleTimeZone.getRawOffset()
"
  (FOIL::CALL-PROP-GET '|java.util|::|SimpleTimeZone| "rawOffset"
                       '|java.util|::SIMPLETIMEZONE.RAWOFFSET-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.RAW-OFFSET-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.SimpleTimeZone.setRawOffset(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|SimpleTimeZone| "rawOffset"
                       '|java.util|::SIMPLETIMEZONE.RAWOFFSET-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN (SETF |java.util|::SIMPLETIMEZONE.START-YEAR-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.SimpleTimeZone.setStartYear(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|SimpleTimeZone| "startYear"
                       '|java.util|::SIMPLETIMEZONE.STARTYEAR-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFCONSTANT |java.util.spi|::CALENDARDATAPROVIDER.
  '|java.util.spi|::|CalendarDataProvider|)
(DEFCLASS |java.util.spi|::CALENDARDATAPROVIDER.
          (|java.util.spi|::LOCALESERVICEPROVIDER.) NIL)
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.GET-FIRST-DAY-OF-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int java.util.spi.CalendarDataProvider.getFirstDayOfWeek(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider|
                          "getFirstDayOfWeek"
                          '|java.util.spi|::CALENDARDATAPROVIDER.GET-FIRST-DAY-OF-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.GET-MINIMAL-DAYS-IN-FIRST-WEEK
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int java.util.spi.CalendarDataProvider.getMinimalDaysInFirstWeek(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider|
                          "getMinimalDaysInFirstWeek"
                          '|java.util.spi|::CALENDARDATAPROVIDER.GET-MINIMAL-DAYS-IN-FIRST-WEEK
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.IS-SUPPORTED-LOCALE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.spi.LocaleServiceProvider.isSupportedLocale(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider|
                          "isSupportedLocale"
                          '|java.util.spi|::CALENDARDATAPROVIDER.IS-SUPPORTED-LOCALE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.GET-AVAILABLE-LOCALES
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Locale[] java.util.spi.LocaleServiceProvider.getAvailableLocales()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider|
                          "getAvailableLocales"
                          '|java.util.spi|::CALENDARDATAPROVIDER.GET-AVAILABLE-LOCALES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider| "wait"
                          '|java.util.spi|::CALENDARDATAPROVIDER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider| "equals"
                          '|java.util.spi|::CALENDARDATAPROVIDER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider| "toString"
                          '|java.util.spi|::CALENDARDATAPROVIDER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider| "hashCode"
                          '|java.util.spi|::CALENDARDATAPROVIDER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider| "getClass"
                          '|java.util.spi|::CALENDARDATAPROVIDER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider| "notify"
                          '|java.util.spi|::CALENDARDATAPROVIDER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarDataProvider| "notifyAll"
                          '|java.util.spi|::CALENDARDATAPROVIDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.AVAILABLE-LOCALES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Locale[] java.util.spi.LocaleServiceProvider.getAvailableLocales()
"
  (FOIL::CALL-PROP-GET '|java.util.spi|::|CalendarDataProvider|
                       "availableLocales"
                       '|java.util.spi|::CALENDARDATAPROVIDER.AVAILABLELOCALES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARDATAPROVIDER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util.spi|::|CalendarDataProvider| "class"
                       '|java.util.spi|::CALENDARDATAPROVIDER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util.spi|::CALENDARNAMEPROVIDER.
  '|java.util.spi|::|CalendarNameProvider|)
(DEFCLASS |java.util.spi|::CALENDARNAMEPROVIDER.
          (|java.util.spi|::LOCALESERVICEPROVIDER.) NIL)
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.GET-DISPLAY-NAMES
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Map java.util.spi.CalendarNameProvider.getDisplayNames(java.lang.String,int,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider|
                          "getDisplayNames"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.GET-DISPLAY-NAMES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.GET-DISPLAY-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.String java.util.spi.CalendarNameProvider.getDisplayName(java.lang.String,int,int,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider|
                          "getDisplayName"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.GET-DISPLAY-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.IS-SUPPORTED-LOCALE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.spi.LocaleServiceProvider.isSupportedLocale(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider|
                          "isSupportedLocale"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.IS-SUPPORTED-LOCALE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.GET-AVAILABLE-LOCALES
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Locale[] java.util.spi.LocaleServiceProvider.getAvailableLocales()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider|
                          "getAvailableLocales"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.GET-AVAILABLE-LOCALES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider| "wait"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider| "equals"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider| "toString"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider| "hashCode"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider| "getClass"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider| "notify"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|CalendarNameProvider| "notifyAll"
                          '|java.util.spi|::CALENDARNAMEPROVIDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.AVAILABLE-LOCALES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Locale[] java.util.spi.LocaleServiceProvider.getAvailableLocales()
"
  (FOIL::CALL-PROP-GET '|java.util.spi|::|CalendarNameProvider|
                       "availableLocales"
                       '|java.util.spi|::CALENDARNAMEPROVIDER.AVAILABLELOCALES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::CALENDARNAMEPROVIDER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util.spi|::|CalendarNameProvider| "class"
                       '|java.util.spi|::CALENDARNAMEPROVIDER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util.spi|::TIMEZONENAMEPROVIDER.
  '|java.util.spi|::|TimeZoneNameProvider|)
(DEFCLASS |java.util.spi|::TIMEZONENAMEPROVIDER.
          (|java.util.spi|::LOCALESERVICEPROVIDER.) NIL)
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.GET-GENERIC-DISPLAY-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.spi.TimeZoneNameProvider.getGenericDisplayName(java.lang.String,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider|
                          "getGenericDisplayName"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.GET-GENERIC-DISPLAY-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.GET-DISPLAY-NAME
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.lang.String java.util.spi.TimeZoneNameProvider.getDisplayName(java.lang.String,boolean,int,java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider|
                          "getDisplayName"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.GET-DISPLAY-NAME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.IS-SUPPORTED-LOCALE
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.spi.LocaleServiceProvider.isSupportedLocale(java.util.Locale)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider|
                          "isSupportedLocale"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.IS-SUPPORTED-LOCALE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.GET-AVAILABLE-LOCALES
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Locale[] java.util.spi.LocaleServiceProvider.getAvailableLocales()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider|
                          "getAvailableLocales"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.GET-AVAILABLE-LOCALES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider| "wait"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.EQUALS
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider| "equals"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.EQUALS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.TO-STRING
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider| "toString"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.TO-STRING
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.HASH-CODE
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider| "hashCode"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.HASH-CODE
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.GET-CLASS
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider| "getClass"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.GET-CLASS
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.NOTIFY
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider| "notify"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.NOTIFY
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.NOTIFY-ALL
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util.spi|::|TimeZoneNameProvider| "notifyAll"
                          '|java.util.spi|::TIMEZONENAMEPROVIDER.NOTIFY-ALL
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.AVAILABLE-LOCALES-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public abstract java.util.Locale[] java.util.spi.LocaleServiceProvider.getAvailableLocales()
"
  (FOIL::CALL-PROP-GET '|java.util.spi|::|TimeZoneNameProvider|
                       "availableLocales"
                       '|java.util.spi|::TIMEZONENAMEPROVIDER.AVAILABLELOCALES-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN |java.util.spi|::TIMEZONENAMEPROVIDER.CLASS-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util.spi|::|TimeZoneNameProvider| "class"
                       '|java.util.spi|::TIMEZONENAMEPROVIDER.CLASS-GET
                       FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util|::TIMER. '|java.util|::|Timer|)
(DEFCLASS |java.util|::TIMER. (|java.lang|::OBJECT.) NIL)
(DEFUN |java.util|::TIMER.NEW (&REST FOIL::ARGS)
  "public java.util.Timer(java.lang.String,boolean)
public java.util.Timer(java.lang.String)
public java.util.Timer(boolean)
public java.util.Timer()
"
  (FOIL::CALL-CTOR '|java.util|::|Timer| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM (EQL (QUOTE |java.util|::|Timer|)))
            &REST FOIL::ARGS)
  (APPLY #'|java.util|::TIMER.NEW FOIL::ARGS))
(DEFUN |java.util|::TIMER.SCHEDULE (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Timer.schedule(java.util.TimerTask,java.util.Date,long)
public void java.util.Timer.schedule(java.util.TimerTask,long,long)
public void java.util.Timer.schedule(java.util.TimerTask,java.util.Date)
public void java.util.Timer.schedule(java.util.TimerTask,long)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "schedule"
                          '|java.util|::TIMER.SCHEDULE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.SCHEDULE-AT-FIXED-RATE (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Timer.scheduleAtFixedRate(java.util.TimerTask,long,long)
public void java.util.Timer.scheduleAtFixedRate(java.util.TimerTask,java.util.Date,long)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "scheduleAtFixedRate"
                          '|java.util|::TIMER.SCHEDULE-AT-FIXED-RATE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMER.CANCEL (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.Timer.cancel()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "cancel"
                          '|java.util|::TIMER.CANCEL FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.PURGE (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.Timer.purge()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "purge"
                          '|java.util|::TIMER.PURGE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "wait" '|java.util|::TIMER.WAIT
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "equals"
                          '|java.util|::TIMER.EQUALS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "toString"
                          '|java.util|::TIMER.TO-STRING FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "hashCode"
                          '|java.util|::TIMER.HASH-CODE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "getClass"
                          '|java.util|::TIMER.GET-CLASS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "notify"
                          '|java.util|::TIMER.NOTIFY FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|Timer| "notifyAll"
                          '|java.util|::TIMER.NOTIFY-ALL FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMER.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|Timer| "class"
                       '|java.util|::TIMER.CLASS-GET FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util|::TIMERTASK. '|java.util|::|TimerTask|)
(DEFCLASS |java.util|::TIMERTASK. (|java.lang|::RUNNABLE. |java.lang|::OBJECT.)
          NIL)
(DEFUN |java.util|::TIMERTASK.CANCEL (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.TimerTask.cancel()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "cancel"
                          '|java.util|::TIMERTASK.CANCEL FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.SCHEDULED-EXECUTION-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public long java.util.TimerTask.scheduledExecutionTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "scheduledExecutionTime"
                          '|java.util|::TIMERTASK.SCHEDULED-EXECUTION-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.RUN (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void java.util.TimerTask.run()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "run"
                          '|java.util|::TIMERTASK.RUN FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "wait"
                          '|java.util|::TIMERTASK.WAIT FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "equals"
                          '|java.util|::TIMERTASK.EQUALS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "toString"
                          '|java.util|::TIMERTASK.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "hashCode"
                          '|java.util|::TIMERTASK.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "getClass"
                          '|java.util|::TIMERTASK.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "notify"
                          '|java.util|::TIMERTASK.NOTIFY FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerTask| "notifyAll"
                          '|java.util|::TIMERTASK.NOTIFY-ALL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTASK.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerTask| "class"
                       '|java.util|::TIMERTASK.CLASS-GET FOIL::THIS FOIL::ARGS))
(DEFCONSTANT |java.util|::TIMERTHREAD. '|java.util|::|TimerThread|)
(DEFCLASS |java.util|::TIMERTHREAD. (|java.lang|::THREAD.) NIL)
(DEFUN |java.util|::TIMERTHREAD.RUN (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.TimerThread.run()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "run"
                          '|java.util|::TIMERTHREAD.RUN FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Thread.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "toString"
                          '|java.util|::TIMERTHREAD.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.IS-INTERRUPTED (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Thread.isInterrupted()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "isInterrupted"
                          '|java.util|::TIMERTHREAD.IS-INTERRUPTED FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.CURRENT-THREAD (&REST FOIL::ARGS)
  "public static native java.lang.Thread java.lang.Thread.currentThread()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "currentThread"
                          '|java.util|::TIMERTHREAD.CURRENT-THREAD NIL
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-NAME (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.lang.Thread.getName()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getName"
                          '|java.util|::TIMERTHREAD.GET-NAME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.JOIN (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.join() throws java.lang.InterruptedException
public final synchronized void java.lang.Thread.join(long,int) throws java.lang.InterruptedException
public final synchronized void java.lang.Thread.join(long) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "join"
                          '|java.util|::TIMERTHREAD.JOIN FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-THREAD-GROUP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.ThreadGroup java.lang.Thread.getThreadGroup()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getThreadGroup"
                          '|java.util|::TIMERTHREAD.GET-THREAD-GROUP FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-STACK-TRACE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.StackTraceElement[] java.lang.Thread.getStackTrace()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getStackTrace"
                          '|java.util|::TIMERTHREAD.GET-STACK-TRACE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.HOLDS-LOCK (&REST FOIL::ARGS)
  "public static native boolean java.lang.Thread.holdsLock(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "holdsLock"
                          '|java.util|::TIMERTHREAD.HOLDS-LOCK NIL FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.CHECK-ACCESS (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.checkAccess()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "checkAccess"
                          '|java.util|::TIMERTHREAD.CHECK-ACCESS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.DUMP-STACK (&REST FOIL::ARGS)
  "public static void java.lang.Thread.dumpStack()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "dumpStack"
                          '|java.util|::TIMERTHREAD.DUMP-STACK NIL FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.SET-PRIORITY (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.setPriority(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "setPriority"
                          '|java.util|::TIMERTHREAD.SET-PRIORITY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.SET-DAEMON (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.setDaemon(boolean)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "setDaemon"
                          '|java.util|::TIMERTHREAD.SET-DAEMON FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.START (FOIL::THIS &REST FOIL::ARGS)
  "public synchronized void java.lang.Thread.start()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "start"
                          '|java.util|::TIMERTHREAD.START FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.YIELD (&REST FOIL::ARGS)
  "public static native void java.lang.Thread.yield()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "yield"
                          '|java.util|::TIMERTHREAD.YIELD NIL FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.SLEEP (&REST FOIL::ARGS)
  "public static void java.lang.Thread.sleep(long,int) throws java.lang.InterruptedException
public static native void java.lang.Thread.sleep(long) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "sleep"
                          '|java.util|::TIMERTHREAD.SLEEP NIL FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.STOP (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.stop()
public final synchronized void java.lang.Thread.stop(java.lang.Throwable)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "stop"
                          '|java.util|::TIMERTHREAD.STOP FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.INTERRUPT (FOIL::THIS &REST FOIL::ARGS)
  "public void java.lang.Thread.interrupt()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "interrupt"
                          '|java.util|::TIMERTHREAD.INTERRUPT FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.INTERRUPTED (&REST FOIL::ARGS)
  "public static boolean java.lang.Thread.interrupted()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "interrupted"
                          '|java.util|::TIMERTHREAD.INTERRUPTED NIL FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.DESTROY (FOIL::THIS &REST FOIL::ARGS)
  "public void java.lang.Thread.destroy()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "destroy"
                          '|java.util|::TIMERTHREAD.DESTROY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.IS-ALIVE (FOIL::THIS &REST FOIL::ARGS)
  "public final native boolean java.lang.Thread.isAlive()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "isAlive"
                          '|java.util|::TIMERTHREAD.IS-ALIVE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.SUSPEND (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.suspend()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "suspend"
                          '|java.util|::TIMERTHREAD.SUSPEND FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.RESUME (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.resume()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "resume"
                          '|java.util|::TIMERTHREAD.RESUME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-PRIORITY (FOIL::THIS &REST FOIL::ARGS)
  "public final int java.lang.Thread.getPriority()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getPriority"
                          '|java.util|::TIMERTHREAD.GET-PRIORITY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.SET-NAME (FOIL::THIS &REST FOIL::ARGS)
  "public final synchronized void java.lang.Thread.setName(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "setName"
                          '|java.util|::TIMERTHREAD.SET-NAME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.ACTIVE-COUNT (&REST FOIL::ARGS)
  "public static int java.lang.Thread.activeCount()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "activeCount"
                          '|java.util|::TIMERTHREAD.ACTIVE-COUNT NIL
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.ENUMERATE (&REST FOIL::ARGS)
  "public static int java.lang.Thread.enumerate(java.lang.Thread[])
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "enumerate"
                          '|java.util|::TIMERTHREAD.ENUMERATE NIL FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.COUNT-STACK-FRAMES
       (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Thread.countStackFrames()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "countStackFrames"
                          '|java.util|::TIMERTHREAD.COUNT-STACK-FRAMES
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.IS-DAEMON (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean java.lang.Thread.isDaemon()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "isDaemon"
                          '|java.util|::TIMERTHREAD.IS-DAEMON FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-CONTEXT-CLASS-LOADER
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.ClassLoader java.lang.Thread.getContextClassLoader()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getContextClassLoader"
                          '|java.util|::TIMERTHREAD.GET-CONTEXT-CLASS-LOADER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.SET-CONTEXT-CLASS-LOADER
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.lang.Thread.setContextClassLoader(java.lang.ClassLoader)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "setContextClassLoader"
                          '|java.util|::TIMERTHREAD.SET-CONTEXT-CLASS-LOADER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-ALL-STACK-TRACES (&REST FOIL::ARGS)
  "public static java.util.Map java.lang.Thread.getAllStackTraces()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getAllStackTraces"
                          '|java.util|::TIMERTHREAD.GET-ALL-STACK-TRACES NIL
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-ID (FOIL::THIS &REST FOIL::ARGS)
  "public long java.lang.Thread.getId()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getId"
                          '|java.util|::TIMERTHREAD.GET-ID FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-STATE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Thread$State java.lang.Thread.getState()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getState"
                          '|java.util|::TIMERTHREAD.GET-STATE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.SET-DEFAULT-UNCAUGHT-EXCEPTION-HANDLER
       (&REST FOIL::ARGS)
  "public static void java.lang.Thread.setDefaultUncaughtExceptionHandler(java.lang.Thread$UncaughtExceptionHandler)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread|
                          "setDefaultUncaughtExceptionHandler"
                          '|java.util|::TIMERTHREAD.SET-DEFAULT-UNCAUGHT-EXCEPTION-HANDLER
                          NIL FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-DEFAULT-UNCAUGHT-EXCEPTION-HANDLER
       (&REST FOIL::ARGS)
  "public static java.lang.Thread$UncaughtExceptionHandler java.lang.Thread.getDefaultUncaughtExceptionHandler()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread|
                          "getDefaultUncaughtExceptionHandler"
                          '|java.util|::TIMERTHREAD.GET-DEFAULT-UNCAUGHT-EXCEPTION-HANDLER
                          NIL FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-UNCAUGHT-EXCEPTION-HANDLER
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Thread$UncaughtExceptionHandler java.lang.Thread.getUncaughtExceptionHandler()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread|
                          "getUncaughtExceptionHandler"
                          '|java.util|::TIMERTHREAD.GET-UNCAUGHT-EXCEPTION-HANDLER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.SET-UNCAUGHT-EXCEPTION-HANDLER
       (FOIL::THIS &REST FOIL::ARGS)
  "public void java.lang.Thread.setUncaughtExceptionHandler(java.lang.Thread$UncaughtExceptionHandler)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread|
                          "setUncaughtExceptionHandler"
                          '|java.util|::TIMERTHREAD.SET-UNCAUGHT-EXCEPTION-HANDLER
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "wait"
                          '|java.util|::TIMERTHREAD.WAIT FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "equals"
                          '|java.util|::TIMERTHREAD.EQUALS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "hashCode"
                          '|java.util|::TIMERTHREAD.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "getClass"
                          '|java.util|::TIMERTHREAD.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "notify"
                          '|java.util|::TIMERTHREAD.NOTIFY FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimerThread| "notifyAll"
                          '|java.util|::TIMERTHREAD.NOTIFY-ALL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.+MIN_PRIORITY+ ()
  "public static final int java.lang.Thread.MIN_PRIORITY"
  (FOIL::CALL-FIELD '|java.util|::|TimerThread| "MIN_PRIORITY"
                    '|java.util|::TIMERTHREAD.MIN_PRIORITY* NIL))
(DEFUN (SETF |java.util|::TIMERTHREAD.+MIN_PRIORITY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|TimerThread| "MIN_PRIORITY"
                    '|java.util|::TIMERTHREAD.MIN_PRIORITY* NIL FOIL::VAL))
(DEFUN |java.util|::TIMERTHREAD.+NORM_PRIORITY+ ()
  "public static final int java.lang.Thread.NORM_PRIORITY"
  (FOIL::CALL-FIELD '|java.util|::|TimerThread| "NORM_PRIORITY"
                    '|java.util|::TIMERTHREAD.NORM_PRIORITY* NIL))
(DEFUN (SETF |java.util|::TIMERTHREAD.+NORM_PRIORITY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|TimerThread| "NORM_PRIORITY"
                    '|java.util|::TIMERTHREAD.NORM_PRIORITY* NIL FOIL::VAL))
(DEFUN |java.util|::TIMERTHREAD.+MAX_PRIORITY+ ()
  "public static final int java.lang.Thread.MAX_PRIORITY"
  (FOIL::CALL-FIELD '|java.util|::|TimerThread| "MAX_PRIORITY"
                    '|java.util|::TIMERTHREAD.MAX_PRIORITY* NIL))
(DEFUN (SETF |java.util|::TIMERTHREAD.+MAX_PRIORITY+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|TimerThread| "MAX_PRIORITY"
                    '|java.util|::TIMERTHREAD.MAX_PRIORITY* NIL FOIL::VAL))
(DEFUN |java.util|::TIMERTHREAD.ALIVE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native boolean java.lang.Thread.isAlive()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "alive"
                       '|java.util|::TIMERTHREAD.ALIVE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "class"
                       '|java.util|::TIMERTHREAD.CLASS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.CONTEXT-CLASS-LOADER-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.ClassLoader java.lang.Thread.getContextClassLoader()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "contextClassLoader"
                       '|java.util|::TIMERTHREAD.CONTEXTCLASSLOADER-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::TIMERTHREAD.CONTEXT-CLASS-LOADER-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.lang.Thread.setContextClassLoader(java.lang.ClassLoader)
"
  (FOIL::CALL-PROP-SET '|java.util|::|TimerThread| "contextClassLoader"
                       '|java.util|::TIMERTHREAD.CONTEXTCLASSLOADER-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::TIMERTHREAD.DAEMON-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final boolean java.lang.Thread.isDaemon()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "daemon"
                       '|java.util|::TIMERTHREAD.DAEMON-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::TIMERTHREAD.DAEMON-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.setDaemon(boolean)
"
  (FOIL::CALL-PROP-SET '|java.util|::|TimerThread| "daemon"
                       '|java.util|::TIMERTHREAD.DAEMON-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::TIMERTHREAD.ID-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public long java.lang.Thread.getId()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "id"
                       '|java.util|::TIMERTHREAD.ID-GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.INTERRUPTED-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Thread.isInterrupted()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "interrupted"
                       '|java.util|::TIMERTHREAD.INTERRUPTED-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.NAME-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.lang.Thread.getName()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "name"
                       '|java.util|::TIMERTHREAD.NAME-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::TIMERTHREAD.NAME-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public final synchronized void java.lang.Thread.setName(java.lang.String)
"
  (FOIL::CALL-PROP-SET '|java.util|::|TimerThread| "name"
                       '|java.util|::TIMERTHREAD.NAME-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::TIMERTHREAD.PRIORITY-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final int java.lang.Thread.getPriority()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "priority"
                       '|java.util|::TIMERTHREAD.PRIORITY-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::TIMERTHREAD.PRIORITY-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Thread.setPriority(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|TimerThread| "priority"
                       '|java.util|::TIMERTHREAD.PRIORITY-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::TIMERTHREAD.STACK-TRACE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.StackTraceElement[] java.lang.Thread.getStackTrace()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "stackTrace"
                       '|java.util|::TIMERTHREAD.STACKTRACE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.STATE-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Thread$State java.lang.Thread.getState()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "state"
                       '|java.util|::TIMERTHREAD.STATE-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.THREAD-GROUP-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.ThreadGroup java.lang.Thread.getThreadGroup()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "threadGroup"
                       '|java.util|::TIMERTHREAD.THREADGROUP-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::TIMERTHREAD.UNCAUGHT-EXCEPTION-HANDLER-PROP
       (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Thread$UncaughtExceptionHandler java.lang.Thread.getUncaughtExceptionHandler()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimerThread| "uncaughtExceptionHandler"
                       '|java.util|::TIMERTHREAD.UNCAUGHTEXCEPTIONHANDLER-GET
                       FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::TIMERTHREAD.UNCAUGHT-EXCEPTION-HANDLER-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.lang.Thread.setUncaughtExceptionHandler(java.lang.Thread$UncaughtExceptionHandler)
"
  (FOIL::CALL-PROP-SET '|java.util|::|TimerThread| "uncaughtExceptionHandler"
                       '|java.util|::TIMERTHREAD.UNCAUGHTEXCEPTIONHANDLER-SET
                       FOIL::THIS (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFCONSTANT |java.util|::TIMEZONE. '|java.util|::|TimeZone|)
(DEFCLASS |java.util|::TIMEZONE.
          (|java.io|::SERIALIZABLE. |java.lang|::CLONEABLE.
           |java.lang|::OBJECT.)
          NIL)
(DEFUN |java.util|::TIMEZONE.NEW (&REST FOIL::ARGS)
  "public java.util.TimeZone()
"
  (FOIL::CALL-CTOR '|java.util|::|TimeZone| FOIL::ARGS))
(DEFMETHOD FOIL:MAKE-NEW
           ((FOIL::CLASS-SYM (EQL (QUOTE |java.util|::|TimeZone|)))
            &REST FOIL::ARGS)
  (APPLY #'|java.util|::TIMEZONE.NEW FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.IN-DAYLIGHT-TIME (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean java.util.TimeZone.inDaylightTime(java.util.Date)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "inDaylightTime"
                          '|java.util|::TIMEZONE.IN-DAYLIGHT-TIME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-RAW-OFFSET (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int java.util.TimeZone.getRawOffset()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getRawOffset"
                          '|java.util|::TIMEZONE.GET-RAW-OFFSET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-DST-SAVINGS (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.TimeZone.getDSTSavings()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getDSTSavings"
                          '|java.util|::TIMEZONE.GET-DST-SAVINGS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.HAS-SAME-RULES (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.TimeZone.hasSameRules(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "hasSameRules"
                          '|java.util|::TIMEZONE.HAS-SAME-RULES FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.SET-RAW-OFFSET (FOIL::THIS &REST FOIL::ARGS)
  "public abstract void java.util.TimeZone.setRawOffset(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "setRawOffset"
                          '|java.util|::TIMEZONE.SET-RAW-OFFSET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.SET-ID (FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.TimeZone.setID(java.lang.String)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "setID"
                          '|java.util|::TIMEZONE.SET-ID FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.USE-DAYLIGHT-TIME (FOIL::THIS &REST FOIL::ARGS)
  "public abstract boolean java.util.TimeZone.useDaylightTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "useDaylightTime"
                          '|java.util|::TIMEZONE.USE-DAYLIGHT-TIME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.OBSERVES-DAYLIGHT-TIME
       (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.util.TimeZone.observesDaylightTime()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "observesDaylightTime"
                          '|java.util|::TIMEZONE.OBSERVES-DAYLIGHT-TIME
                          FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-AVAILABLE-I-DS (&REST FOIL::ARGS)
  "public static synchronized java.lang.String[] java.util.TimeZone.getAvailableIDs()
public static synchronized java.lang.String[] java.util.TimeZone.getAvailableIDs(int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getAvailableIDs"
                          '|java.util|::TIMEZONE.GET-AVAILABLE-I-DS NIL
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.TO-ZONE-ID (FOIL::THIS &REST FOIL::ARGS)
  "public java.time.ZoneId java.util.TimeZone.toZoneId()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "toZoneId"
                          '|java.util|::TIMEZONE.TO-ZONE-ID FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-TIME-ZONE (&REST FOIL::ARGS)
  "public static synchronized java.util.TimeZone java.util.TimeZone.getTimeZone(java.lang.String)
public static java.util.TimeZone java.util.TimeZone.getTimeZone(java.time.ZoneId)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getTimeZone"
                          '|java.util|::TIMEZONE.GET-TIME-ZONE NIL FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.CLONE (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.Object java.util.TimeZone.clone()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "clone"
                          '|java.util|::TIMEZONE.CLONE FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-DEFAULT (&REST FOIL::ARGS)
  "public static java.util.TimeZone java.util.TimeZone.getDefault()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getDefault"
                          '|java.util|::TIMEZONE.GET-DEFAULT NIL FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-OFFSET (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.TimeZone.getOffset(long)
public abstract int java.util.TimeZone.getOffset(int,int,int,int,int,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getOffset"
                          '|java.util|::TIMEZONE.GET-OFFSET FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.SET-DEFAULT (&REST FOIL::ARGS)
  "public static void java.util.TimeZone.setDefault(java.util.TimeZone)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "setDefault"
                          '|java.util|::TIMEZONE.SET-DEFAULT NIL FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-DISPLAY-NAME (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.TimeZone.getDisplayName(boolean,int,java.util.Locale)
public final java.lang.String java.util.TimeZone.getDisplayName()
public final java.lang.String java.util.TimeZone.getDisplayName(java.util.Locale)
public final java.lang.String java.util.TimeZone.getDisplayName(boolean,int)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getDisplayName"
                          '|java.util|::TIMEZONE.GET-DISPLAY-NAME FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-ID (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.TimeZone.getID()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getID"
                          '|java.util|::TIMEZONE.GET-ID FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.WAIT (FOIL::THIS &REST FOIL::ARGS)
  "public final void java.lang.Object.wait() throws java.lang.InterruptedException
public final native void java.lang.Object.wait(long) throws java.lang.InterruptedException
public final void java.lang.Object.wait(long,int) throws java.lang.InterruptedException
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "wait"
                          '|java.util|::TIMEZONE.WAIT FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.EQUALS (FOIL::THIS &REST FOIL::ARGS)
  "public boolean java.lang.Object.equals(java.lang.Object)
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "equals"
                          '|java.util|::TIMEZONE.EQUALS FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.TO-STRING (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.lang.Object.toString()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "toString"
                          '|java.util|::TIMEZONE.TO-STRING FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.HASH-CODE (FOIL::THIS &REST FOIL::ARGS)
  "public native int java.lang.Object.hashCode()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "hashCode"
                          '|java.util|::TIMEZONE.HASH-CODE FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.GET-CLASS (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "getClass"
                          '|java.util|::TIMEZONE.GET-CLASS FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.NOTIFY (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notify()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "notify"
                          '|java.util|::TIMEZONE.NOTIFY FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.NOTIFY-ALL (FOIL::THIS &REST FOIL::ARGS)
  "public final native void java.lang.Object.notifyAll()
"
  (FOIL::FOIL-CALL-METHOD '|java.util|::|TimeZone| "notifyAll"
                          '|java.util|::TIMEZONE.NOTIFY-ALL FOIL::THIS
                          FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.+SHORT+ ()
  "public static final int java.util.TimeZone.SHORT"
  (FOIL::CALL-FIELD '|java.util|::|TimeZone| "SHORT"
                    '|java.util|::TIMEZONE.SHORT* NIL))
(DEFUN (SETF |java.util|::TIMEZONE.+SHORT+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|TimeZone| "SHORT"
                    '|java.util|::TIMEZONE.SHORT* NIL FOIL::VAL))
(DEFUN |java.util|::TIMEZONE.+LONG+ ()
  "public static final int java.util.TimeZone.LONG"
  (FOIL::CALL-FIELD '|java.util|::|TimeZone| "LONG"
                    '|java.util|::TIMEZONE.LONG* NIL))
(DEFUN (SETF |java.util|::TIMEZONE.+LONG+) (FOIL::VAL)
  (FOIL::CALL-FIELD '|java.util|::|TimeZone| "LONG"
                    '|java.util|::TIMEZONE.LONG* NIL FOIL::VAL))
(DEFUN |java.util|::TIMEZONE.DST-SAVINGS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public int java.util.TimeZone.getDSTSavings()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimeZone| "DSTSavings"
                       '|java.util|::TIMEZONE.DSTSAVINGS-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.ID-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public java.lang.String java.util.TimeZone.getID()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimeZone| "ID"
                       '|java.util|::TIMEZONE.ID-GET FOIL::THIS FOIL::ARGS))
(DEFUN (SETF |java.util|::TIMEZONE.ID-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public void java.util.TimeZone.setID(java.lang.String)
"
  (FOIL::CALL-PROP-SET '|java.util|::|TimeZone| "ID"
                       '|java.util|::TIMEZONE.ID-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(DEFUN |java.util|::TIMEZONE.CLASS-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final native java.lang.Class java.lang.Object.getClass()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimeZone| "class"
                       '|java.util|::TIMEZONE.CLASS-GET FOIL::THIS FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.DISPLAY-NAME-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public final java.lang.String java.util.TimeZone.getDisplayName()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimeZone| "displayName"
                       '|java.util|::TIMEZONE.DISPLAYNAME-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN |java.util|::TIMEZONE.RAW-OFFSET-PROP (FOIL::THIS &REST FOIL::ARGS)
  "public abstract int java.util.TimeZone.getRawOffset()
"
  (FOIL::CALL-PROP-GET '|java.util|::|TimeZone| "rawOffset"
                       '|java.util|::TIMEZONE.RAWOFFSET-GET FOIL::THIS
                       FOIL::ARGS))
(DEFUN (SETF |java.util|::TIMEZONE.RAW-OFFSET-PROP)
       (FOIL::VAL FOIL::THIS &REST FOIL::ARGS)
  "public abstract void java.util.TimeZone.setRawOffset(int)
"
  (FOIL::CALL-PROP-SET '|java.util|::|TimeZone| "rawOffset"
                       '|java.util|::TIMEZONE.RAWOFFSET-SET FOIL::THIS
                       (APPEND FOIL::ARGS (LIST FOIL::VAL))))
(eval-when (:load-toplevel)(export '(|java.util|::TIMEZONE.RAW-OFFSET-PROP
                                     |java.util|::TIMEZONE.DISPLAY-NAME-PROP
                                     |java.util|::TIMEZONE.CLASS-PROP
                                     |java.util|::TIMEZONE.ID-PROP
                                     |java.util|::TIMEZONE.DST-SAVINGS-PROP
                                     |java.util|::TIMEZONE.+LONG+
                                     |java.util|::TIMEZONE.+SHORT+
                                     |java.util|::TIMEZONE.NOTIFY-ALL
                                     |java.util|::TIMEZONE.NOTIFY
                                     |java.util|::TIMEZONE.GET-CLASS
                                     |java.util|::TIMEZONE.HASH-CODE
                                     |java.util|::TIMEZONE.TO-STRING
                                     |java.util|::TIMEZONE.EQUALS
                                     |java.util|::TIMEZONE.WAIT
                                     |java.util|::TIMEZONE.GET-ID
                                     |java.util|::TIMEZONE.GET-DISPLAY-NAME
                                     |java.util|::TIMEZONE.SET-DEFAULT
                                     |java.util|::TIMEZONE.GET-OFFSET
                                     |java.util|::TIMEZONE.GET-DEFAULT
                                     |java.util|::TIMEZONE.CLONE
                                     |java.util|::TIMEZONE.GET-TIME-ZONE
                                     |java.util|::TIMEZONE.TO-ZONE-ID
                                     |java.util|::TIMEZONE.GET-AVAILABLE-I-DS
                                     |java.util|::TIMEZONE.OBSERVES-DAYLIGHT-TIME
                                     |java.util|::TIMEZONE.USE-DAYLIGHT-TIME
                                     |java.util|::TIMEZONE.SET-ID
                                     |java.util|::TIMEZONE.SET-RAW-OFFSET
                                     |java.util|::TIMEZONE.HAS-SAME-RULES
                                     |java.util|::TIMEZONE.GET-DST-SAVINGS
                                     |java.util|::TIMEZONE.GET-RAW-OFFSET
                                     |java.util|::TIMEZONE.IN-DAYLIGHT-TIME
                                     |java.util|::TIMEZONE.NEW
                                     |java.util|::TIMEZONE.
                                     |java.util|::TIMERTHREAD.UNCAUGHT-EXCEPTION-HANDLER-PROP
                                     |java.util|::TIMERTHREAD.THREAD-GROUP-PROP
                                     |java.util|::TIMERTHREAD.STATE-PROP
                                     |java.util|::TIMERTHREAD.STACK-TRACE-PROP
                                     |java.util|::TIMERTHREAD.PRIORITY-PROP
                                     |java.util|::TIMERTHREAD.NAME-PROP
                                     |java.util|::TIMERTHREAD.INTERRUPTED-PROP
                                     |java.util|::TIMERTHREAD.ID-PROP
                                     |java.util|::TIMERTHREAD.DAEMON-PROP
                                     |java.util|::TIMERTHREAD.CONTEXT-CLASS-LOADER-PROP
                                     |java.util|::TIMERTHREAD.CLASS-PROP
                                     |java.util|::TIMERTHREAD.ALIVE-PROP
                                     |java.util|::TIMERTHREAD.+MAX_PRIORITY+
                                     |java.util|::TIMERTHREAD.+NORM_PRIORITY+
                                     |java.util|::TIMERTHREAD.+MIN_PRIORITY+
                                     |java.util|::TIMERTHREAD.NOTIFY-ALL
                                     |java.util|::TIMERTHREAD.NOTIFY
                                     |java.util|::TIMERTHREAD.GET-CLASS
                                     |java.util|::TIMERTHREAD.HASH-CODE
                                     |java.util|::TIMERTHREAD.EQUALS
                                     |java.util|::TIMERTHREAD.WAIT
                                     |java.util|::TIMERTHREAD.SET-UNCAUGHT-EXCEPTION-HANDLER
                                     |java.util|::TIMERTHREAD.GET-UNCAUGHT-EXCEPTION-HANDLER
                                     |java.util|::TIMERTHREAD.GET-DEFAULT-UNCAUGHT-EXCEPTION-HANDLER
                                     |java.util|::TIMERTHREAD.SET-DEFAULT-UNCAUGHT-EXCEPTION-HANDLER
                                     |java.util|::TIMERTHREAD.GET-STATE
                                     |java.util|::TIMERTHREAD.GET-ID
                                     |java.util|::TIMERTHREAD.GET-ALL-STACK-TRACES
                                     |java.util|::TIMERTHREAD.SET-CONTEXT-CLASS-LOADER
                                     |java.util|::TIMERTHREAD.GET-CONTEXT-CLASS-LOADER
                                     |java.util|::TIMERTHREAD.IS-DAEMON
                                     |java.util|::TIMERTHREAD.COUNT-STACK-FRAMES
                                     |java.util|::TIMERTHREAD.ENUMERATE
                                     |java.util|::TIMERTHREAD.ACTIVE-COUNT
                                     |java.util|::TIMERTHREAD.SET-NAME
                                     |java.util|::TIMERTHREAD.GET-PRIORITY
                                     |java.util|::TIMERTHREAD.RESUME
                                     |java.util|::TIMERTHREAD.SUSPEND
                                     |java.util|::TIMERTHREAD.IS-ALIVE
                                     |java.util|::TIMERTHREAD.DESTROY
                                     |java.util|::TIMERTHREAD.INTERRUPTED
                                     |java.util|::TIMERTHREAD.INTERRUPT
                                     |java.util|::TIMERTHREAD.STOP
                                     |java.util|::TIMERTHREAD.SLEEP
                                     |java.util|::TIMERTHREAD.YIELD
                                     |java.util|::TIMERTHREAD.START
                                     |java.util|::TIMERTHREAD.SET-DAEMON
                                     |java.util|::TIMERTHREAD.SET-PRIORITY
                                     |java.util|::TIMERTHREAD.DUMP-STACK
                                     |java.util|::TIMERTHREAD.CHECK-ACCESS
                                     |java.util|::TIMERTHREAD.HOLDS-LOCK
                                     |java.util|::TIMERTHREAD.GET-STACK-TRACE
                                     |java.util|::TIMERTHREAD.GET-THREAD-GROUP
                                     |java.util|::TIMERTHREAD.JOIN
                                     |java.util|::TIMERTHREAD.GET-NAME
                                     |java.util|::TIMERTHREAD.CURRENT-THREAD
                                     |java.util|::TIMERTHREAD.IS-INTERRUPTED
                                     |java.util|::TIMERTHREAD.TO-STRING
                                     |java.util|::TIMERTHREAD.RUN
                                     |java.util|::TIMERTHREAD.
                                     |java.util|::TIMERTASK.CLASS-PROP
                                     |java.util|::TIMERTASK.NOTIFY-ALL
                                     |java.util|::TIMERTASK.NOTIFY
                                     |java.util|::TIMERTASK.GET-CLASS
                                     |java.util|::TIMERTASK.HASH-CODE
                                     |java.util|::TIMERTASK.TO-STRING
                                     |java.util|::TIMERTASK.EQUALS
                                     |java.util|::TIMERTASK.WAIT
                                     |java.util|::TIMERTASK.RUN
                                     |java.util|::TIMERTASK.SCHEDULED-EXECUTION-TIME
                                     |java.util|::TIMERTASK.CANCEL
                                     |java.util|::TIMERTASK.
                                     |java.util|::TIMER.CLASS-PROP
                                     |java.util|::TIMER.NOTIFY-ALL
                                     |java.util|::TIMER.NOTIFY
                                     |java.util|::TIMER.GET-CLASS
                                     |java.util|::TIMER.HASH-CODE
                                     |java.util|::TIMER.TO-STRING
                                     |java.util|::TIMER.EQUALS
                                     |java.util|::TIMER.WAIT
                                     |java.util|::TIMER.PURGE
                                     |java.util|::TIMER.CANCEL
                                     |java.util|::TIMER.SCHEDULE-AT-FIXED-RATE
                                     |java.util|::TIMER.SCHEDULE
                                     |java.util|::TIMER.NEW |java.util|::TIMER.
                                     |java.util|::SIMPLETIMEZONE.START-YEAR-PROP
                                     |java.util|::SIMPLETIMEZONE.RAW-OFFSET-PROP
                                     |java.util|::SIMPLETIMEZONE.DISPLAY-NAME-PROP
                                     |java.util|::SIMPLETIMEZONE.CLASS-PROP
                                     |java.util|::SIMPLETIMEZONE.ID-PROP
                                     |java.util|::SIMPLETIMEZONE.DST-SAVINGS-PROP
                                     |java.util|::SIMPLETIMEZONE.+LONG+
                                     |java.util|::SIMPLETIMEZONE.+SHORT+
                                     |java.util|::SIMPLETIMEZONE.+UTC_TIME+
                                     |java.util|::SIMPLETIMEZONE.+STANDARD_TIME+
                                     |java.util|::SIMPLETIMEZONE.+WALL_TIME+
                                     |java.util|::SIMPLETIMEZONE.NOTIFY-ALL
                                     |java.util|::SIMPLETIMEZONE.NOTIFY
                                     |java.util|::SIMPLETIMEZONE.GET-CLASS
                                     |java.util|::SIMPLETIMEZONE.WAIT
                                     |java.util|::SIMPLETIMEZONE.GET-ID
                                     |java.util|::SIMPLETIMEZONE.GET-DISPLAY-NAME
                                     |java.util|::SIMPLETIMEZONE.SET-DEFAULT
                                     |java.util|::SIMPLETIMEZONE.GET-DEFAULT
                                     |java.util|::SIMPLETIMEZONE.GET-TIME-ZONE
                                     |java.util|::SIMPLETIMEZONE.TO-ZONE-ID
                                     |java.util|::SIMPLETIMEZONE.GET-AVAILABLE-I-DS
                                     |java.util|::SIMPLETIMEZONE.SET-ID
                                     |java.util|::SIMPLETIMEZONE.GET-OFFSET
                                     |java.util|::SIMPLETIMEZONE.CLONE
                                     |java.util|::SIMPLETIMEZONE.HASH-CODE
                                     |java.util|::SIMPLETIMEZONE.TO-STRING
                                     |java.util|::SIMPLETIMEZONE.EQUALS
                                     |java.util|::SIMPLETIMEZONE.SET-DST-SAVINGS
                                     |java.util|::SIMPLETIMEZONE.SET-END-RULE
                                     |java.util|::SIMPLETIMEZONE.SET-START-RULE
                                     |java.util|::SIMPLETIMEZONE.SET-START-YEAR
                                     |java.util|::SIMPLETIMEZONE.OBSERVES-DAYLIGHT-TIME
                                     |java.util|::SIMPLETIMEZONE.USE-DAYLIGHT-TIME
                                     |java.util|::SIMPLETIMEZONE.SET-RAW-OFFSET
                                     |java.util|::SIMPLETIMEZONE.HAS-SAME-RULES
                                     |java.util|::SIMPLETIMEZONE.GET-DST-SAVINGS
                                     |java.util|::SIMPLETIMEZONE.GET-RAW-OFFSET
                                     |java.util|::SIMPLETIMEZONE.IN-DAYLIGHT-TIME
                                     |java.util|::SIMPLETIMEZONE.NEW
                                     |java.util|::SIMPLETIMEZONE.
                                     |java.util|::JAPANESEIMPERIALCALENDAR.WEEKS-IN-WEEK-YEAR-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.WEEK-YEAR-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.WEEK-DATE-SUPPORTED-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.TIME-ZONE-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.TIME-IN-MILLIS-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.TIME-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.MINIMUM-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.MAXIMUM-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.LENIENT-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.LEAST-MAXIMUM-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GREATEST-MINIMUM-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.FIRST-DAY-OF-WEEK-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.CLASS-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.CALENDAR-TYPE-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.ACTUAL-MINIMUM-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.ACTUAL-MAXIMUM-PROP
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+LONG_STANDALONE+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT_STANDALONE+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+LONG_FORMAT+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT_FORMAT+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+NARROW_STANDALONE+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+NARROW_FORMAT+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+LONG+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+SHORT+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+ALL_STYLES+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+PM+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+AM+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+UNDECIMBER+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+DECEMBER+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+NOVEMBER+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+OCTOBER+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+SEPTEMBER+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+AUGUST+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+JULY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+JUNE+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+MAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+APRIL+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+MARCH+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+FEBRUARY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+JANUARY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+SATURDAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+FRIDAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+THURSDAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+WEDNESDAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+TUESDAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+MONDAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+SUNDAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+FIELD_COUNT+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+DST_OFFSET+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+ZONE_OFFSET+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+MILLISECOND+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+SECOND+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+MINUTE+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+HOUR_OF_DAY+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+HOUR+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+AM_PM+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_WEEK_IN_MONTH+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_WEEK+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_YEAR+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+DAY_OF_MONTH+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+DATE+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+WEEK_OF_MONTH+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+WEEK_OF_YEAR+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+MONTH+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+YEAR+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+ERA+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+HEISEI+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+SHOWA+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+TAISHO+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+MEIJI+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.+BEFORE_MEIJI+
                                     |java.util|::JAPANESEIMPERIALCALENDAR.NOTIFY-ALL
                                     |java.util|::JAPANESEIMPERIALCALENDAR.NOTIFY
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-CLASS
                                     |java.util|::JAPANESEIMPERIALCALENDAR.WAIT
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-AVAILABLE-LOCALES
                                     |java.util|::JAPANESEIMPERIALCALENDAR.AFTER
                                     |java.util|::JAPANESEIMPERIALCALENDAR.BEFORE
                                     |java.util|::JAPANESEIMPERIALCALENDAR.SET
                                     |java.util|::JAPANESEIMPERIALCALENDAR.IS-SET
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-INSTANCE
                                     |java.util|::JAPANESEIMPERIALCALENDAR.CLEAR
                                     |java.util|::JAPANESEIMPERIALCALENDAR.COMPARE-TO
                                     |java.util|::JAPANESEIMPERIALCALENDAR.TO-STRING
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME
                                     |java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME-IN-MILLIS
                                     |java.util|::JAPANESEIMPERIALCALENDAR.TO-INSTANT
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-WEEKS-IN-WEEK-YEAR
                                     |java.util|::JAPANESEIMPERIALCALENDAR.SET-WEEK-DATE
                                     |java.util|::JAPANESEIMPERIALCALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
                                     |java.util|::JAPANESEIMPERIALCALENDAR.SET-FIRST-DAY-OF-WEEK
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-AVAILABLE-CALENDAR-TYPES
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-FIRST-DAY-OF-WEEK
                                     |java.util|::JAPANESEIMPERIALCALENDAR.IS-LENIENT
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-WEEK-YEAR
                                     |java.util|::JAPANESEIMPERIALCALENDAR.IS-WEEK-DATE-SUPPORTED
                                     |java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME-IN-MILLIS
                                     |java.util|::JAPANESEIMPERIALCALENDAR.SET-LENIENT
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-DISPLAY-NAME
                                     |java.util|::JAPANESEIMPERIALCALENDAR.CLONE
                                     |java.util|::JAPANESEIMPERIALCALENDAR.HASH-CODE
                                     |java.util|::JAPANESEIMPERIALCALENDAR.EQUALS
                                     |java.util|::JAPANESEIMPERIALCALENDAR.ADD
                                     |java.util|::JAPANESEIMPERIALCALENDAR.SET-TIME-ZONE
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-TIME-ZONE
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-CALENDAR-TYPE
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-MAXIMUM
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-MINIMUM
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-ACTUAL-MAXIMUM
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-ACTUAL-MINIMUM
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-GREATEST-MINIMUM
                                     |java.util|::JAPANESEIMPERIALCALENDAR.ROLL
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-DISPLAY-NAMES
                                     |java.util|::JAPANESEIMPERIALCALENDAR.GET-LEAST-MAXIMUM
                                     |java.util|::JAPANESEIMPERIALCALENDAR.
                                     |java.util|::GREGORIANCALENDAR.WEEKS-IN-WEEK-YEAR-PROP
                                     |java.util|::GREGORIANCALENDAR.WEEK-YEAR-PROP
                                     |java.util|::GREGORIANCALENDAR.WEEK-DATE-SUPPORTED-PROP
                                     |java.util|::GREGORIANCALENDAR.TIME-ZONE-PROP
                                     |java.util|::GREGORIANCALENDAR.TIME-IN-MILLIS-PROP
                                     |java.util|::GREGORIANCALENDAR.TIME-PROP
                                     |java.util|::GREGORIANCALENDAR.MINIMUM-PROP
                                     |java.util|::GREGORIANCALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP
                                     |java.util|::GREGORIANCALENDAR.MAXIMUM-PROP
                                     |java.util|::GREGORIANCALENDAR.LENIENT-PROP
                                     |java.util|::GREGORIANCALENDAR.LEAST-MAXIMUM-PROP
                                     |java.util|::GREGORIANCALENDAR.GREGORIAN-CHANGE-PROP
                                     |java.util|::GREGORIANCALENDAR.GREATEST-MINIMUM-PROP
                                     |java.util|::GREGORIANCALENDAR.FIRST-DAY-OF-WEEK-PROP
                                     |java.util|::GREGORIANCALENDAR.CLASS-PROP
                                     |java.util|::GREGORIANCALENDAR.CALENDAR-TYPE-PROP
                                     |java.util|::GREGORIANCALENDAR.ACTUAL-MINIMUM-PROP
                                     |java.util|::GREGORIANCALENDAR.ACTUAL-MAXIMUM-PROP
                                     |java.util|::GREGORIANCALENDAR.+LONG_STANDALONE+
                                     |java.util|::GREGORIANCALENDAR.+SHORT_STANDALONE+
                                     |java.util|::GREGORIANCALENDAR.+LONG_FORMAT+
                                     |java.util|::GREGORIANCALENDAR.+SHORT_FORMAT+
                                     |java.util|::GREGORIANCALENDAR.+NARROW_STANDALONE+
                                     |java.util|::GREGORIANCALENDAR.+NARROW_FORMAT+
                                     |java.util|::GREGORIANCALENDAR.+LONG+
                                     |java.util|::GREGORIANCALENDAR.+SHORT+
                                     |java.util|::GREGORIANCALENDAR.+ALL_STYLES+
                                     |java.util|::GREGORIANCALENDAR.+PM+
                                     |java.util|::GREGORIANCALENDAR.+AM+
                                     |java.util|::GREGORIANCALENDAR.+UNDECIMBER+
                                     |java.util|::GREGORIANCALENDAR.+DECEMBER+
                                     |java.util|::GREGORIANCALENDAR.+NOVEMBER+
                                     |java.util|::GREGORIANCALENDAR.+OCTOBER+
                                     |java.util|::GREGORIANCALENDAR.+SEPTEMBER+
                                     |java.util|::GREGORIANCALENDAR.+AUGUST+
                                     |java.util|::GREGORIANCALENDAR.+JULY+
                                     |java.util|::GREGORIANCALENDAR.+JUNE+
                                     |java.util|::GREGORIANCALENDAR.+MAY+
                                     |java.util|::GREGORIANCALENDAR.+APRIL+
                                     |java.util|::GREGORIANCALENDAR.+MARCH+
                                     |java.util|::GREGORIANCALENDAR.+FEBRUARY+
                                     |java.util|::GREGORIANCALENDAR.+JANUARY+
                                     |java.util|::GREGORIANCALENDAR.+SATURDAY+
                                     |java.util|::GREGORIANCALENDAR.+FRIDAY+
                                     |java.util|::GREGORIANCALENDAR.+THURSDAY+
                                     |java.util|::GREGORIANCALENDAR.+WEDNESDAY+
                                     |java.util|::GREGORIANCALENDAR.+TUESDAY+
                                     |java.util|::GREGORIANCALENDAR.+MONDAY+
                                     |java.util|::GREGORIANCALENDAR.+SUNDAY+
                                     |java.util|::GREGORIANCALENDAR.+FIELD_COUNT+
                                     |java.util|::GREGORIANCALENDAR.+DST_OFFSET+
                                     |java.util|::GREGORIANCALENDAR.+ZONE_OFFSET+
                                     |java.util|::GREGORIANCALENDAR.+MILLISECOND+
                                     |java.util|::GREGORIANCALENDAR.+SECOND+
                                     |java.util|::GREGORIANCALENDAR.+MINUTE+
                                     |java.util|::GREGORIANCALENDAR.+HOUR_OF_DAY+
                                     |java.util|::GREGORIANCALENDAR.+HOUR+
                                     |java.util|::GREGORIANCALENDAR.+AM_PM+
                                     |java.util|::GREGORIANCALENDAR.+DAY_OF_WEEK_IN_MONTH+
                                     |java.util|::GREGORIANCALENDAR.+DAY_OF_WEEK+
                                     |java.util|::GREGORIANCALENDAR.+DAY_OF_YEAR+
                                     |java.util|::GREGORIANCALENDAR.+DAY_OF_MONTH+
                                     |java.util|::GREGORIANCALENDAR.+DATE+
                                     |java.util|::GREGORIANCALENDAR.+WEEK_OF_MONTH+
                                     |java.util|::GREGORIANCALENDAR.+WEEK_OF_YEAR+
                                     |java.util|::GREGORIANCALENDAR.+MONTH+
                                     |java.util|::GREGORIANCALENDAR.+YEAR+
                                     |java.util|::GREGORIANCALENDAR.+ERA+
                                     |java.util|::GREGORIANCALENDAR.+AD+
                                     |java.util|::GREGORIANCALENDAR.+BC+
                                     |java.util|::GREGORIANCALENDAR.NOTIFY-ALL
                                     |java.util|::GREGORIANCALENDAR.NOTIFY
                                     |java.util|::GREGORIANCALENDAR.GET-CLASS
                                     |java.util|::GREGORIANCALENDAR.WAIT
                                     |java.util|::GREGORIANCALENDAR.GET-DISPLAY-NAME
                                     |java.util|::GREGORIANCALENDAR.GET-AVAILABLE-LOCALES
                                     |java.util|::GREGORIANCALENDAR.AFTER
                                     |java.util|::GREGORIANCALENDAR.BEFORE
                                     |java.util|::GREGORIANCALENDAR.SET
                                     |java.util|::GREGORIANCALENDAR.IS-SET
                                     |java.util|::GREGORIANCALENDAR.GET-INSTANCE
                                     |java.util|::GREGORIANCALENDAR.CLEAR
                                     |java.util|::GREGORIANCALENDAR.COMPARE-TO
                                     |java.util|::GREGORIANCALENDAR.TO-STRING
                                     |java.util|::GREGORIANCALENDAR.GET
                                     |java.util|::GREGORIANCALENDAR.GET-TIME
                                     |java.util|::GREGORIANCALENDAR.SET-TIME
                                     |java.util|::GREGORIANCALENDAR.GET-TIME-IN-MILLIS
                                     |java.util|::GREGORIANCALENDAR.TO-INSTANT
                                     |java.util|::GREGORIANCALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
                                     |java.util|::GREGORIANCALENDAR.SET-FIRST-DAY-OF-WEEK
                                     |java.util|::GREGORIANCALENDAR.GET-AVAILABLE-CALENDAR-TYPES
                                     |java.util|::GREGORIANCALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
                                     |java.util|::GREGORIANCALENDAR.GET-FIRST-DAY-OF-WEEK
                                     |java.util|::GREGORIANCALENDAR.IS-LENIENT
                                     |java.util|::GREGORIANCALENDAR.GET-DISPLAY-NAMES
                                     |java.util|::GREGORIANCALENDAR.SET-TIME-IN-MILLIS
                                     |java.util|::GREGORIANCALENDAR.SET-LENIENT
                                     |java.util|::GREGORIANCALENDAR.CLONE
                                     |java.util|::GREGORIANCALENDAR.HASH-CODE
                                     |java.util|::GREGORIANCALENDAR.EQUALS
                                     |java.util|::GREGORIANCALENDAR.ADD
                                     |java.util|::GREGORIANCALENDAR.GET-GREGORIAN-CHANGE
                                     |java.util|::GREGORIANCALENDAR.SET-GREGORIAN-CHANGE
                                     |java.util|::GREGORIANCALENDAR.FROM
                                     |java.util|::GREGORIANCALENDAR.SET-TIME-ZONE
                                     |java.util|::GREGORIANCALENDAR.GET-TIME-ZONE
                                     |java.util|::GREGORIANCALENDAR.GET-CALENDAR-TYPE
                                     |java.util|::GREGORIANCALENDAR.GET-MAXIMUM
                                     |java.util|::GREGORIANCALENDAR.GET-MINIMUM
                                     |java.util|::GREGORIANCALENDAR.IS-LEAP-YEAR
                                     |java.util|::GREGORIANCALENDAR.GET-ACTUAL-MAXIMUM
                                     |java.util|::GREGORIANCALENDAR.GET-ACTUAL-MINIMUM
                                     |java.util|::GREGORIANCALENDAR.GET-GREATEST-MINIMUM
                                     |java.util|::GREGORIANCALENDAR.GET-WEEKS-IN-WEEK-YEAR
                                     |java.util|::GREGORIANCALENDAR.SET-WEEK-DATE
                                     |java.util|::GREGORIANCALENDAR.ROLL
                                     |java.util|::GREGORIANCALENDAR.GET-LEAST-MAXIMUM
                                     |java.util|::GREGORIANCALENDAR.GET-WEEK-YEAR
                                     |java.util|::GREGORIANCALENDAR.IS-WEEK-DATE-SUPPORTED
                                     |java.util|::GREGORIANCALENDAR.TO-ZONED-DATE-TIME
                                     |java.util|::GREGORIANCALENDAR.NEW
                                     |java.util|::GREGORIANCALENDAR.
                                     |java.util|::FORMATTER$DATETIME.CLASS-PROP
                                     |java.util|::FORMATTER$DATETIME.NOTIFY-ALL
                                     |java.util|::FORMATTER$DATETIME.NOTIFY
                                     |java.util|::FORMATTER$DATETIME.GET-CLASS
                                     |java.util|::FORMATTER$DATETIME.HASH-CODE
                                     |java.util|::FORMATTER$DATETIME.TO-STRING
                                     |java.util|::FORMATTER$DATETIME.EQUALS
                                     |java.util|::FORMATTER$DATETIME.WAIT
                                     |java.util|::FORMATTER$DATETIME.
                                     |java.util|::DATE.YEAR-PROP
                                     |java.util|::DATE.TIMEZONE-OFFSET-PROP
                                     |java.util|::DATE.TIME-PROP
                                     |java.util|::DATE.SECONDS-PROP
                                     |java.util|::DATE.MONTH-PROP
                                     |java.util|::DATE.MINUTES-PROP
                                     |java.util|::DATE.HOURS-PROP
                                     |java.util|::DATE.DAY-PROP
                                     |java.util|::DATE.DATE-PROP
                                     |java.util|::DATE.CLASS-PROP
                                     |java.util|::DATE.NOTIFY-ALL
                                     |java.util|::DATE.NOTIFY
                                     |java.util|::DATE.GET-CLASS
                                     |java.util|::DATE.WAIT
                                     |java.util|::DATE.AFTER
                                     |java.util|::DATE.BEFORE
                                     |java.util|::DATE.PARSE
                                     |java.util|::DATE.COMPARE-TO
                                     |java.util|::DATE.CLONE
                                     |java.util|::DATE.HASH-CODE
                                     |java.util|::DATE.TO-STRING
                                     |java.util|::DATE.EQUALS
                                     |java.util|::DATE.GET-YEAR
                                     |java.util|::DATE.FROM
                                     |java.util|::DATE.GET-TIME
                                     |java.util|::DATE.SET-TIME
                                     |java.util|::DATE.GET-DATE
                                     |java.util|::DATE.GET-TIMEZONE-OFFSET
                                     |java.util|::DATE.TO-GMT-STRING
                                     |java.util|::DATE.TO-LOCALE-STRING
                                     |java.util|::DATE.SET-SECONDS
                                     |java.util|::DATE.SET-MINUTES
                                     |java.util|::DATE.GET-MINUTES
                                     |java.util|::DATE.SET-HOURS
                                     |java.util|::DATE.GET-HOURS
                                     |java.util|::DATE.GET-DAY
                                     |java.util|::DATE.SET-DATE
                                     |java.util|::DATE.SET-MONTH
                                     |java.util|::DATE.SET-YEAR
                                     |java.util|::DATE.GET-SECONDS
                                     |java.util|::DATE.TO-INSTANT
                                     |java.util|::DATE.GET-MONTH
                                     |java.util|::DATE.UTC
                                     |java.util|::DATE.NEW |java.util|::DATE.
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.CLASS-PROP
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.NOTIFY-ALL
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.NOTIFY
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.GET-CLASS
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.HASH-CODE
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.TO-STRING
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.EQUALS
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.WAIT
                                     |java.util|::CALENDAR$CALENDARACCESSCONTROLCONTEXT.
                                     |java.util|::CALENDAR$BUILDER.CLASS-PROP
                                     |java.util|::CALENDAR$BUILDER.NOTIFY-ALL
                                     |java.util|::CALENDAR$BUILDER.NOTIFY
                                     |java.util|::CALENDAR$BUILDER.GET-CLASS
                                     |java.util|::CALENDAR$BUILDER.HASH-CODE
                                     |java.util|::CALENDAR$BUILDER.TO-STRING
                                     |java.util|::CALENDAR$BUILDER.EQUALS
                                     |java.util|::CALENDAR$BUILDER.WAIT
                                     |java.util|::CALENDAR$BUILDER.SET
                                     |java.util|::CALENDAR$BUILDER.SET-WEEK-DEFINITION
                                     |java.util|::CALENDAR$BUILDER.SET-CALENDAR-TYPE
                                     |java.util|::CALENDAR$BUILDER.SET-FIELDS
                                     |java.util|::CALENDAR$BUILDER.SET-INSTANT
                                     |java.util|::CALENDAR$BUILDER.SET-LOCALE
                                     |java.util|::CALENDAR$BUILDER.SET-TIME-ZONE
                                     |java.util|::CALENDAR$BUILDER.SET-TIME-OF-DAY
                                     |java.util|::CALENDAR$BUILDER.SET-DATE
                                     |java.util|::CALENDAR$BUILDER.SET-WEEK-DATE
                                     |java.util|::CALENDAR$BUILDER.SET-LENIENT
                                     |java.util|::CALENDAR$BUILDER.BUILD
                                     |java.util|::CALENDAR$BUILDER.NEW
                                     |java.util|::CALENDAR$BUILDER.
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.CLASS-PROP
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.NOTIFY-ALL
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.NOTIFY
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.GET-CLASS
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.HASH-CODE
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.TO-STRING
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.EQUALS
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.WAIT
                                     |java.util|::CALENDAR$AVAILABLECALENDARTYPES.
                                     |java.util|::CALENDAR.WEEKS-IN-WEEK-YEAR-PROP
                                     |java.util|::CALENDAR.WEEK-YEAR-PROP
                                     |java.util|::CALENDAR.WEEK-DATE-SUPPORTED-PROP
                                     |java.util|::CALENDAR.TIME-ZONE-PROP
                                     |java.util|::CALENDAR.TIME-IN-MILLIS-PROP
                                     |java.util|::CALENDAR.TIME-PROP
                                     |java.util|::CALENDAR.MINIMUM-PROP
                                     |java.util|::CALENDAR.MINIMAL-DAYS-IN-FIRST-WEEK-PROP
                                     |java.util|::CALENDAR.MAXIMUM-PROP
                                     |java.util|::CALENDAR.LENIENT-PROP
                                     |java.util|::CALENDAR.LEAST-MAXIMUM-PROP
                                     |java.util|::CALENDAR.GREATEST-MINIMUM-PROP
                                     |java.util|::CALENDAR.FIRST-DAY-OF-WEEK-PROP
                                     |java.util|::CALENDAR.CLASS-PROP
                                     |java.util|::CALENDAR.CALENDAR-TYPE-PROP
                                     |java.util|::CALENDAR.ACTUAL-MINIMUM-PROP
                                     |java.util|::CALENDAR.ACTUAL-MAXIMUM-PROP
                                     |java.util|::CALENDAR.+LONG_STANDALONE+
                                     |java.util|::CALENDAR.+SHORT_STANDALONE+
                                     |java.util|::CALENDAR.+LONG_FORMAT+
                                     |java.util|::CALENDAR.+SHORT_FORMAT+
                                     |java.util|::CALENDAR.+NARROW_STANDALONE+
                                     |java.util|::CALENDAR.+NARROW_FORMAT+
                                     |java.util|::CALENDAR.+LONG+
                                     |java.util|::CALENDAR.+SHORT+
                                     |java.util|::CALENDAR.+ALL_STYLES+
                                     |java.util|::CALENDAR.+PM+
                                     |java.util|::CALENDAR.+AM+
                                     |java.util|::CALENDAR.+UNDECIMBER+
                                     |java.util|::CALENDAR.+DECEMBER+
                                     |java.util|::CALENDAR.+NOVEMBER+
                                     |java.util|::CALENDAR.+OCTOBER+
                                     |java.util|::CALENDAR.+SEPTEMBER+
                                     |java.util|::CALENDAR.+AUGUST+
                                     |java.util|::CALENDAR.+JULY+
                                     |java.util|::CALENDAR.+JUNE+
                                     |java.util|::CALENDAR.+MAY+
                                     |java.util|::CALENDAR.+APRIL+
                                     |java.util|::CALENDAR.+MARCH+
                                     |java.util|::CALENDAR.+FEBRUARY+
                                     |java.util|::CALENDAR.+JANUARY+
                                     |java.util|::CALENDAR.+SATURDAY+
                                     |java.util|::CALENDAR.+FRIDAY+
                                     |java.util|::CALENDAR.+THURSDAY+
                                     |java.util|::CALENDAR.+WEDNESDAY+
                                     |java.util|::CALENDAR.+TUESDAY+
                                     |java.util|::CALENDAR.+MONDAY+
                                     |java.util|::CALENDAR.+SUNDAY+
                                     |java.util|::CALENDAR.+FIELD_COUNT+
                                     |java.util|::CALENDAR.+DST_OFFSET+
                                     |java.util|::CALENDAR.+ZONE_OFFSET+
                                     |java.util|::CALENDAR.+MILLISECOND+
                                     |java.util|::CALENDAR.+SECOND+
                                     |java.util|::CALENDAR.+MINUTE+
                                     |java.util|::CALENDAR.+HOUR_OF_DAY+
                                     |java.util|::CALENDAR.+HOUR+
                                     |java.util|::CALENDAR.+AM_PM+
                                     |java.util|::CALENDAR.+DAY_OF_WEEK_IN_MONTH+
                                     |java.util|::CALENDAR.+DAY_OF_WEEK+
                                     |java.util|::CALENDAR.+DAY_OF_YEAR+
                                     |java.util|::CALENDAR.+DAY_OF_MONTH+
                                     |java.util|::CALENDAR.+DATE+
                                     |java.util|::CALENDAR.+WEEK_OF_MONTH+
                                     |java.util|::CALENDAR.+WEEK_OF_YEAR+
                                     |java.util|::CALENDAR.+MONTH+
                                     |java.util|::CALENDAR.+YEAR+
                                     |java.util|::CALENDAR.+ERA+
                                     |java.util|::CALENDAR.NOTIFY-ALL
                                     |java.util|::CALENDAR.NOTIFY
                                     |java.util|::CALENDAR.GET-CLASS
                                     |java.util|::CALENDAR.WAIT
                                     |java.util|::CALENDAR.GET-DISPLAY-NAME
                                     |java.util|::CALENDAR.GET-AVAILABLE-LOCALES
                                     |java.util|::CALENDAR.AFTER
                                     |java.util|::CALENDAR.BEFORE
                                     |java.util|::CALENDAR.SET
                                     |java.util|::CALENDAR.IS-SET
                                     |java.util|::CALENDAR.GET-INSTANCE
                                     |java.util|::CALENDAR.CLEAR
                                     |java.util|::CALENDAR.COMPARE-TO
                                     |java.util|::CALENDAR.CLONE
                                     |java.util|::CALENDAR.HASH-CODE
                                     |java.util|::CALENDAR.TO-STRING
                                     |java.util|::CALENDAR.EQUALS
                                     |java.util|::CALENDAR.GET
                                     |java.util|::CALENDAR.ADD
                                     |java.util|::CALENDAR.GET-TIME
                                     |java.util|::CALENDAR.SET-TIME
                                     |java.util|::CALENDAR.SET-TIME-ZONE
                                     |java.util|::CALENDAR.GET-TIME-ZONE
                                     |java.util|::CALENDAR.GET-TIME-IN-MILLIS
                                     |java.util|::CALENDAR.GET-CALENDAR-TYPE
                                     |java.util|::CALENDAR.GET-MAXIMUM
                                     |java.util|::CALENDAR.GET-MINIMUM
                                     |java.util|::CALENDAR.TO-INSTANT
                                     |java.util|::CALENDAR.GET-ACTUAL-MAXIMUM
                                     |java.util|::CALENDAR.GET-ACTUAL-MINIMUM
                                     |java.util|::CALENDAR.GET-GREATEST-MINIMUM
                                     |java.util|::CALENDAR.GET-WEEKS-IN-WEEK-YEAR
                                     |java.util|::CALENDAR.SET-WEEK-DATE
                                     |java.util|::CALENDAR.SET-MINIMAL-DAYS-IN-FIRST-WEEK
                                     |java.util|::CALENDAR.SET-FIRST-DAY-OF-WEEK
                                     |java.util|::CALENDAR.ROLL
                                     |java.util|::CALENDAR.GET-AVAILABLE-CALENDAR-TYPES
                                     |java.util|::CALENDAR.GET-MINIMAL-DAYS-IN-FIRST-WEEK
                                     |java.util|::CALENDAR.GET-FIRST-DAY-OF-WEEK
                                     |java.util|::CALENDAR.IS-LENIENT
                                     |java.util|::CALENDAR.GET-DISPLAY-NAMES
                                     |java.util|::CALENDAR.GET-LEAST-MAXIMUM
                                     |java.util|::CALENDAR.GET-WEEK-YEAR
                                     |java.util|::CALENDAR.IS-WEEK-DATE-SUPPORTED
                                     |java.util|::CALENDAR.SET-TIME-IN-MILLIS
                                     |java.util|::CALENDAR.SET-LENIENT
                                     |java.util|::CALENDAR.) "java.util"))
(eval-when (:load-toplevel)(export '(|java.util.spi|::TIMEZONENAMEPROVIDER.CLASS-PROP
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.AVAILABLE-LOCALES-PROP
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.NOTIFY-ALL
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.NOTIFY
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.GET-CLASS
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.HASH-CODE
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.TO-STRING
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.EQUALS
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.WAIT
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.GET-AVAILABLE-LOCALES
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.IS-SUPPORTED-LOCALE
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.GET-DISPLAY-NAME
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.GET-GENERIC-DISPLAY-NAME
                                     |java.util.spi|::TIMEZONENAMEPROVIDER.
                                     |java.util.spi|::CALENDARNAMEPROVIDER.CLASS-PROP
                                     |java.util.spi|::CALENDARNAMEPROVIDER.AVAILABLE-LOCALES-PROP
                                     |java.util.spi|::CALENDARNAMEPROVIDER.NOTIFY-ALL
                                     |java.util.spi|::CALENDARNAMEPROVIDER.NOTIFY
                                     |java.util.spi|::CALENDARNAMEPROVIDER.GET-CLASS
                                     |java.util.spi|::CALENDARNAMEPROVIDER.HASH-CODE
                                     |java.util.spi|::CALENDARNAMEPROVIDER.TO-STRING
                                     |java.util.spi|::CALENDARNAMEPROVIDER.EQUALS
                                     |java.util.spi|::CALENDARNAMEPROVIDER.WAIT
                                     |java.util.spi|::CALENDARNAMEPROVIDER.GET-AVAILABLE-LOCALES
                                     |java.util.spi|::CALENDARNAMEPROVIDER.IS-SUPPORTED-LOCALE
                                     |java.util.spi|::CALENDARNAMEPROVIDER.GET-DISPLAY-NAME
                                     |java.util.spi|::CALENDARNAMEPROVIDER.GET-DISPLAY-NAMES
                                     |java.util.spi|::CALENDARNAMEPROVIDER.
                                     |java.util.spi|::CALENDARDATAPROVIDER.CLASS-PROP
                                     |java.util.spi|::CALENDARDATAPROVIDER.AVAILABLE-LOCALES-PROP
                                     |java.util.spi|::CALENDARDATAPROVIDER.NOTIFY-ALL
                                     |java.util.spi|::CALENDARDATAPROVIDER.NOTIFY
                                     |java.util.spi|::CALENDARDATAPROVIDER.GET-CLASS
                                     |java.util.spi|::CALENDARDATAPROVIDER.HASH-CODE
                                     |java.util.spi|::CALENDARDATAPROVIDER.TO-STRING
                                     |java.util.spi|::CALENDARDATAPROVIDER.EQUALS
                                     |java.util.spi|::CALENDARDATAPROVIDER.WAIT
                                     |java.util.spi|::CALENDARDATAPROVIDER.GET-AVAILABLE-LOCALES
                                     |java.util.spi|::CALENDARDATAPROVIDER.IS-SUPPORTED-LOCALE
                                     |java.util.spi|::CALENDARDATAPROVIDER.GET-MINIMAL-DAYS-IN-FIRST-WEEK
                                     |java.util.spi|::CALENDARDATAPROVIDER.GET-FIRST-DAY-OF-WEEK
                                     |java.util.spi|::CALENDARDATAPROVIDER.) "java.util.spi"))
